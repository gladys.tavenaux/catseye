<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;


function fputcsv2($fh, array $fields, $delimiter = ',', $enclosure = '"', $mysql_null = false) {
    $delimiter_esc = preg_quote($delimiter, '/');
    $enclosure_esc = preg_quote($enclosure, '/');

    $output = array();
    foreach ($fields as $field) {
        if ($field === null && $mysql_null) {
            $output[] = 'NULL';
            continue;
        }

        $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
            $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
        ) : $field;
    }

    fwrite($fh, join($delimiter, $output) . "\n");
}

// the _EXACT_ LOAD DATA INFILE command to use
// (if you pass in something different for $delimiter
// and/or $enclosure above, change them here too;
// but _LEAVE ESCAPED BY EMPTY!_).
/*
LOAD DATA INFILE
    '/path/to/file.csv'

INTO TABLE
    my_table

FIELDS TERMINATED BY
    ','

OPTIONALLY ENCLOSED BY
    '"'

ESCAPED BY
    ''

LINES TERMINATED BY
    '\n'
*/



if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{


$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
// create new tables
//$sqlite->createTables(); Crée une vieille table de tutoriel (SQLiteCreateTable.php)
// get the table list
$tables = $sqlite->getTableList();
$shifts = $sqlite->getShifts($_SESSION['id']);
$employees = $sqlite->getEmployees();
$shiftsOfEmployee = $sqlite->getShiftsForId($_SESSION["id"]);
$allShifts = $sqlite->getAllShifts();
$estChef = $sqlite->EstChef($_SESSION['id']);
$estAdmin = $sqlite->EstAdmin($_SESSION['id']);


if(isset($_POST['envoyer'])){
  if ($_POST['envoyer']=='perso'){
    $result=$shiftsOfEmployee;
  }
  elseif ($_POST['envoyer']=='team') {
    if($estChef==true){
      $result=$shifts;
    }
  }
  elseif ($_POST['envoyer']=='admin') {
    if($estAdmin==true){
      $result=$allShifts;
    }
  }
  else {
    $result=$shiftsOfEmployee;
  }

  $fp = fopen('file.csv', 'w');

  foreach ($result as $fields) {
      fputcsv2($fp, $fields);
  }



$file = 'file.csv';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    fclose($fp);
    exit;
}
else{  echo '<script type="text/javascript">window.alert("Erreur");</script>';}

}


/*                         Transfère en PDF                           */
if(isset($_POST['pdf'])){
require('app/fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

if ($_POST['pdf']=='perso'){
  $result=$shiftsOfEmployee;

  $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
  $pdf->Cell(30,10,'',0,1,'C');

    $pdf->Cell(25,10,'Date',1);
    $pdf->Cell(35,10,'Debut de shift',1);
    $pdf->Cell(35,10,'Fin de shift',1);
    $pdf->Cell(30,10,'Duree du shift',1);
    $pdf->Cell(30,10,'Temp de pause',1);
    $pdf->Cell(35,10,'Temp de formation',1,1,'C');

    $pdf->SetFont('Arial','',9);

  foreach ($result as $row) {
    $pdf->Cell(25,10,$row['date'],1);
    $pdf->Cell(35,10,$row['debut'],1);
    $pdf->Cell(35,10,$row['fin'],1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
    $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
  }
}
elseif ($_POST['pdf']=='team') {
  if($estChef==true){$result=$shifts;
$pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'equipe par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
elseif ($_POST['pdf']=='admin') {
  if($estAdmin==true){
    $result=$allShifts;
    $pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'admin par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
  else {
    $result=$shiftsOfEmployee;

    $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');

      $pdf->Cell(25,10,'Date',1);
      $pdf->Cell(35,10,'Debut de shift',1);
      $pdf->Cell(35,10,'Fin de shift',1);
      $pdf->Cell(30,10,'Duree du shift',1);
      $pdf->Cell(30,10,'Temp de pause',1);
      $pdf->Cell(35,10,'Temp de formation',1,1,'C');

      $pdf->SetFont('Arial','',9);

    foreach ($result as $row) {
      $pdf->Cell(25,10,$row['date'],1);
      $pdf->Cell(35,10,$row['debut'],1);
      $pdf->Cell(35,10,$row['fin'],1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
      $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
    }
  }

$pdf->Output();
}

?>

<style>
  body{
    background-image: url("ressources/img6b.jpg"), linear-gradient(#858686, #090909);
    background-repeat: no-repeat;
  }

</style>

<script type="text/javascript">

/** Fonction basculant la visibilité d'un élément dom
* @parameter anId string l'identificateur de la cible à montrer, cacher
*/
function toggle(anId)
{
node = document.getElementById(anId);
if (node.style.visibility=="hidden")
{
// Contenu caché, le montrer
node.style.visibility = "visible";
node.style.height = "auto";			// Optionnel rétablir la hauteur
}
else
{
// Contenu visible, le cacher
node.style.visibility = "hidden";
node.style.height = "0";			// Optionnel libérer l'espace
}
}

</script>




<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys && PELLEMOINE Martin">
        <title>Datas pointeuse</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body>
        <div class="container">
            <div class="page-header"></br>
                <h1>BASE DE DONNEES POINTEUSE</h1></br></br>
            </div>
            <div style="float: right; border: 2px solid black; margin-top: 0; margin-bottom: 2em;
            background: radial-gradient(#919191, #848484, #6F6F6F, #4D4D4D, #2E2E2E,#010101); ">
              <div style="margin: 10px 10px 0 10px;">
                <a href="recompenses.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Voir les récompenses</a>
              </div><br>
              <div style="margin: 0 10px 10px 10px; text-align: center;">
                <a href="rdv.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Gérer mes RDV</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="emploidutemps.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Gérer mon planning</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="dashboard.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Dashboard</a>
              </div>
              <?php if($estChef || $estAdmin){ ?>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="AjouterUser.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Gèrer les utilisateurs</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="Modifier.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Modifier des données</a>
              </div>
            <?php } ?>
            <div style="margin: 20px 10px 10px 10px; text-align: center;">
              <a href="deconnexion.php" class="lienPages" style="padding: 2px 2px 2px 2px; background: radial-gradient(#E8325C, #B51338);">Déconnexion</a>
            </div>
            </div>

            <h2><?php echo "Pour l'utilisateur: " . $_SESSION["name"] ?></h2>


            <h5 style="margin-left: 3em;"><?php
            if($estChef==true)
            {echo "Chef d'équipe";}
            else {
              if($estAdmin==true){echo"<div>Admin";
              echo'<a href="Admin.php" class="lienPages" style="padding: 2px 2px 2px 2px;margin-left:2em;">Gestion</a></div>';}
              else {echo "Agent";}
            }?></h5>
          </br></br><div style="margin:0 0 0 0">
            <!--Télécharger les données de la pointeuse en csv puis pdf -->
            <table class="no-border"><tr><td style="width: 25em;">
              <form action="pointeuse.php" method="POST">
              <input type="hidden" name="envoyer" value="perso">
              <input type="submit" value="Télécharger ces données en CSV">
            </form></td>
            <td><form action="pointeuse.php" method="POST">
              <input type="hidden" name="pdf" value="perso">
              <input type="submit" value="Télécharger ces données en PDF">
            </form></td></tr></table></div>
            <!--Fin du bouton pour dl les données-->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="colonne">Date shift</th>
                        <th class="colonne">Début du shift</th>
                        <th class="colonne">Fin du shift</th>
                        <th class="colonne">Durée du shift</th>
                        <th class="colonne">Durée des pauses</th>
                        <th class="colonne">Durée de formation QA</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                          <?php foreach ($shiftsOfEmployee as $shift) : ?>
                            <td class="case"><?php echo $shift['date'] ?></td>
                            <td class="case"><?php echo $shift['debut'] ?></td>
                            <td class="case"><?php echo $shift['fin'] ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['shift_duration']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['pause_duration']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['qa_formation_duration']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
<!--                             Cacher la suite si l'user n'est pas chef                                   -->

<?php
if($estChef==true){?><br><br>
            <h2><?php echo "Pour l'équipe de l'utilisateur: " . $_SESSION["name"]; ?></h2><br>

<table clas="no-border"><tr><td style="width: 25em;">
            <form action="pointeuse.php" method="POST">
              <input type="hidden" name="envoyer" value="team">
              <input type="submit" value="Télécharger ces données en CSV">
            </form></td>
            <td><form action="pointeuse.php" method="POST">
              <input type="hidden" name="pdf" value="team">
              <input type="submit" value="Télécharger ces données en PDF">
            </form></td></tr></table>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="colonne">Agent</th>
                        <th class="colonne">Id agent</th>
                        <th class="colonne">Date shift</th>
                        <th class="colonne">Début du shift</th>
                        <th class="colonne">Fin du shift</th>
                        <th class="colonne">Durée du shift</th>
                        <th class="colonne">Durée des pauses</th>
                        <th class="colonne">Durée de la formation QA</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                          <?php foreach ($shifts as $shift) : ?>
                          <td class="case"><?php echo $shift['Pseudo'] ?></td>
                            <td class="case"><?php echo $shift['id'] ?></td>
                            <td class="case"><?php echo $shift['date'] ?></td>
                            <td class="case"><?php echo $shift['debut'] ?></td>
                            <td class="case"><?php echo $shift['fin'] ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['durée']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['temps de pause']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['Formation QA']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>


<?php }
elseif ($estAdmin==true){ ?>
      <table class="no-border"><tr><td style="width: 25em;">
              <form action="pointeuse.php" method="POST">
                <input type="hidden" name="envoyer" value="admin">
                <input type="submit" value="Télécharger ces données en CSV">
              </form></td>
              <td><form action="pointeuse.php" method="POST">
                <input type="hidden" name="pdf" value="admin">
                <input type="submit" value="Télécharger ces données en PDF">
              </form></td></tr></table>

              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonne">Agent</th>
                          <th class="colonne">Id agent</th>
                          <th class="colonne">Date shift</th>
                          <th class="colonne">Début du shift</th>
                          <th class="colonne">Fin du shift</th>
                          <th class="colonne">Durée du shift</th>
                          <th class="colonne">Durée des pauses</th>
                          <th class="colonne">Durée de la formation QA</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($allShifts as $shift) : ?>
                            <td class="case"><?php echo $shift['Pseudo'] ?></td>
                              <td class="case"><?php echo $shift['id'] ?></td>
                              <td class="case"><?php echo $shift['date'] ?></td>
                              <td class="case"><?php echo $shift['debut'] ?></td>
                              <td class="case"><?php echo $shift['fin'] ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['durée']) ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['temps de pause']) ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['Formation QA']) ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
<?php } ?>
        </div>
      <?php }
      else {
        header('Location: index.php');
        exit();
      } ?>
    </body>
</html>
