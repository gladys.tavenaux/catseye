<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;
use App\PHP4Paie as Paie;

if(isset($_SESSION['id']) && $_SESSION['id'] != null){


$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
$paie =  new Paie((new SQLiteConnection())->connect());

$tables = $sqlite->getTableList();
$employees = $sqlite->getEmployees();
$allShifts = $sqlite->getAllShifts();
$estAdmin = $sqlite->EstAdmin($_SESSION['id']);
$modifs=$sqlite->getModifications();
$users=$sqlite->getAllAboutUsers();


if($estAdmin==true) {


if(isset($_POST['envoyer'])){
  if ($_POST['envoyer']=='perso'){
    $result=$shiftsOfEmployee;
  }
  elseif ($_POST['envoyer']=='team') {
    if($estChef==true){
      $result=$shifts;
    }
  }
  elseif ($_POST['envoyer']=='admin') {
    if($estAdmin==true){
      $result=$allShifts;
    }
  }
  else {
    $result=$shiftsOfEmployee;
  }

  $fp = fopen('file.csv', 'w');

  foreach ($result as $fields) {
      fputcsv2($fp, $fields);
  }



$file = 'file.csv';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    fclose($fp);
    exit;
}
else{  echo '<script type="text/javascript">window.alert("Erreur");</script>';}

}


/*                         Transfère en PDF                           */
if(isset($_POST['pdf'])){
require('app/fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

if ($_POST['pdf']=='perso'){
  $result=$shiftsOfEmployee;

  $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
  $pdf->Cell(30,10,'',0,1,'C');

    $pdf->Cell(25,10,'Date',1);
    $pdf->Cell(35,10,'Debut de shift',1);
    $pdf->Cell(35,10,'Fin de shift',1);
    $pdf->Cell(30,10,'Duree du shift',1);
    $pdf->Cell(30,10,'Temp de pause',1);
    $pdf->Cell(35,10,'Temp de formation',1,1,'C');

    $pdf->SetFont('Arial','',9);

  foreach ($result as $row) {
    $pdf->Cell(25,10,$row['date'],1);
    $pdf->Cell(35,10,$row['debut'],1);
    $pdf->Cell(35,10,$row['fin'],1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
    $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
  }
}
elseif ($_POST['pdf']=='team') {
  if($estChef==true){$result=$shifts;
$pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'equipe par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
elseif ($_POST['pdf']=='admin') {
  if($estAdmin==true){
    $result=$allShifts;
    $pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'admin par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
  else {
    $result=$shiftsOfEmployee;

    $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');

      $pdf->Cell(25,10,'Date',1);
      $pdf->Cell(35,10,'Debut de shift',1);
      $pdf->Cell(35,10,'Fin de shift',1);
      $pdf->Cell(30,10,'Duree du shift',1);
      $pdf->Cell(30,10,'Temp de pause',1);
      $pdf->Cell(35,10,'Temp de formation',1,1,'C');

      $pdf->SetFont('Arial','',9);

    foreach ($result as $row) {
      $pdf->Cell(25,10,$row['date'],1);
      $pdf->Cell(35,10,$row['debut'],1);
      $pdf->Cell(35,10,$row['fin'],1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
      $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
    }
  }

$pdf->Output();
}

if(isset($_POST['preference']) && $_POST['preference'] != null){ //name $i (0 à 13) et value = id de l'user. Envoyé si checked
  $tableau=[];
   foreach ($users as $shift) {
      for ($i=0; $i < 14 ; $i++) {
        if(isset($_POST[$i.$shift['id']])){
          $tableau[]=[$i, $_POST[$i.$shift['id']]];
          //echo '<script>window.alert("Réponse : '.implode(", ",$tableau[$i]).'")</script>';
        }
      }
    }

  foreach ($tableau as $row => $value) {
  /*  echo '<script>window.alert("Réponse : '.$row.', '.$tableau[$row][1].'")</script>';*///Date, i(shift), value(couleur), id
    /*echo '<script>window.alert("Réponse : '.*/$sqlite->InsertPreference($DateLundiProchain, $tableau[$row][0], 'blue', $tableau[$row][1])/*.'")</script>'*/;
  }
}



$CeLundi = date('d/m/Y', strtotime('last monday'));
$DateLundiProchain =date('d/m/Y',strtotime('next monday'));
$DateDebutShifts = date('d/m/Y',strtotime('14 january 2019'));


function SemaineSuivante($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

function SemainePrecedente($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)-($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

if(!isset($DateLundi) || $DateLundi == null)
{$DateLundi = $CeLundi;}

if (isset($_POST['ChoixSemaine']) && $_POST['ChoixSemaine'] !=null){
  $DateLundiProchain = $_POST['semaine'];
  $DateLundi = SemainePrecedente($DateLundiProchain);
}
else{$DateLundiProchain=SemaineSuivante($DateLundi);}
?>

<style>
  body{
    background-image: url("ressources/img6b.jpg");
    background-repeat: no-repeat;
    background-size: cover;
    margin: 0;
    padding: 0;
    border: none;
  }
  div{
    margin: 0;
    padding: 0;
  }

</style>






<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Gestion du site</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body id="body">
        <div class="container">
            <div class="page-header"></br>
                <h1>Gestion du projet</h1></br></br>
            </div>

            <h2><?php echo "Pour l'utilisateur: " . $_SESSION["name"] ?></h2>
            <div style="float: right; border: 2px solid black; margin-top: 0;
            background: radial-gradient(#919191, #848484, #6F6F6F, #4D4D4D, #2E2E2E, #010101); ">
              <div style="margin: 20px 10px 10px 10px;">
                <a href="pointeuse.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Retourner aux données</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="recompenses.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Voir les récompenses</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="rdv.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Gérer mes RDV</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="emploidutemps.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Gérer mon planning</a>
              </div>
              <div style="margin: 20px 10px 10px 10px; text-align: center;">
                <a href="AjouterUser.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Ajouter un utilisateur</a>
              </div>
              <div style="margin: 20px 10px 20px 10px; text-align: center;">
                <a href="Modifier.php" class="lienPages" style="padding: 2px 2px 2px 2px;">Modifier des données</a>
              </div></div>




<!--                             Cacher la suite si l'user n'est pas chef                                   -->
<br><br><h3>Appuyer pour afficher ou cacher les informations</h3>
<br>
<button id=btnDonneesPointeuse onclick="toggle('donneesPointeuse')">Données de la pointeuse</button><br>
<div id=donneesPointeuse>
      <table class="no-border"><tr><td style="width: 25em;">
              <form action="pointeuse.php" method="POST">
                <input type="hidden" name="envoyer" value="admin">
                <input type="submit" value="Télécharger ces données en CSV">
              </form></td>
              <td><form action="pointeuse.php" method="POST">
                <input type="hidden" name="pdf" value="admin">
                <input type="submit" value="Télécharger ces données en PDF">
              </form></td></tr></table>

              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonne">Agent</th>
                          <th class="colonne">Id agent</th>
                          <th class="colonne">Date shift</th>
                          <th class="colonne">Début du shift</th>
                          <th class="colonne">Fin du shift</th>
                          <th class="colonne">Durée du shift</th>
                          <th class="colonne">Durée des pauses</th>
                          <th class="colonne">Durée de la formation QA</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($allShifts as $shift) : ?>
                            <td class="case"><?php echo $shift['Pseudo'] ?></td>
                              <td class="case"><?php echo $shift['id'] ?></td>
                              <td class="case"><?php echo $shift['date'] ?></td>
                              <td class="case"><?php echo $shift['debut'] ?></td>
                              <td class="case"><?php echo $shift['fin'] ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['durée']) ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['temps de pause']) ?></td>
                              <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['Formation QA']) ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
</div id=donneesPointeuse>
<br>
<button id=btnTablesBDD onclick="toggle('tablesBDD')">Tables de la base de données</button>
<br>
<div id=tablesBDD>
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonne">Table</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($tables as $shift) : ?>
                            <td class="case"><?php echo $shift ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
</div id=tablesBDD>
<br>
<button id=btnModifs onclick="toggle('Modifs')">Modifications de la base de données</button>
<br>
<div id=Modifs>
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonne">Date</th>
                          <th class="colonne">Modification</th>
                          <th class="colonne">Ancienne valeur</th>
                          <th class="colonne">Nouvelle valeur</th>
                          <th class="colonne">Emetteur de la modification</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($modifs as $shift) : ?>
                            <td class="case"><?php echo $shift['Date'] ?></td>
                            <td class="case"><?php echo $shift['Modification'] ?></td>
                            <td class="case"><?php echo $shift['Ancienne valeur'] ?></td>
                            <td class="case"><?php echo $shift['Nouvelle valeur'] ?></td>
                            <td class="case"><?php echo $shift['Emetteur de la modification'] ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
</div id=Modifs>
<br>
<button id=btnUsers onclick="toggle('Users')">Utilisateurs</button>
<br>
<div id=Users>
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonne">id</th>
                          <th class="colonne">Username</th>
                          <th class="colonne">Statut</th>
                          <th class="colonne">Points</th>
                          <th class="colonne">Equipe</th>
                          <th class="colonne">Chef ?</th>
                          <th class="colonne">Admin ?</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($users as $shift) : ?>
                            <td class="case"><?php echo $shift['id'] ?></td>
                            <td class="case"><?php echo $shift['username'] ?></td>
                            <td class="case"><?php echo $shift['status'] ?></td>
                            <td class="case"><?php echo $shift['points'] ?></td>
                            <td class="case"><?php echo $shift['Nom_Equipe'] ?></td>
                            <td class="case"><?php echo $shift['Chef'] ?></td>
                            <td class="case"><?php echo $shift['Admin'] ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
</div id=Users>
<br>
<button id=btnRecompenses onclick="toggle('Recompenses')">Récompenses</button>
<br>
<div id=Recompenses>
  <?php $recompense = $sqlite->getRecompenses();
  if(isset($_POST['NewRecompense'])){
    $points=$_POST['points'];
    $rec=$_POST['recompense'];

    if($points>0 && $rec!=""){
      $rep = $sqlite->ModifBDD("New récompense : ".$points.", ".$rec,"",$points.":".$rec,$_SESSION['id']);
      if($rep>0){
        $sqlite->InsertRecompense($points,$rec);
        echo"<script>window.alert('OK !');</script>";
      }
      else{echo('<script>window.alert("Youuups, erreur !\\nModifBDD<0");</script>');}

    }
    else{echo('<script>window.alert("Merci de renseigner les champs (il n\'y en a que 2 ! Fais un effort 😿)");</script>');}
  }


  ?>
  <table class="table table-bordered">
      <thead>
          <tr>
              <th class="colonne" style="width: 30%;">Seuil de points</th>
              <th class="colonne">Récompense</th>
          </tr>
      </thead>
      <tbody>
              <tr>
                <?php foreach ($recompense as $shift) : ?>
                <td class="case"><?php echo $shift['points'] ?></td>
                  <td class="case"><?php echo $shift['recompense'] ?></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
  <div class="form">
        <form method="post" style="width: 15em; margin-left: 2em;">
          <h5>Ajouter une récompense</h5>
          <table class="borderLess">
            <tr>
              <td>Seuil de points :</td>
              <td><input type="text" name="points"></td>
              <td rowspan="2"><input type="submit" name="NewRecompense"></td>
            </tr>
            <tr>
              <td>Récompense :</td>
              <td><input type="text" name="recompense"></td>
            </tr>
          </table>
        </form>
  </div>
</div id=Recompenses>
<br>

<br><br>
<a href="pointeuse.php" class="lienPages">Retour aux données</a><a href="Mathilde.php" class="lienPages" style="margin-left: 1em;">Gestion shifts & paies</a>
<br><br>

        </div>

        </body>



        <script type="text/javascript">
        /** Fonction basculant la visibilité d'un élément dom
        * @parameter anId string l'identificateur de la cible à montrer, cacher
        */
        function toggle(anId)
        {
          node = document.getElementById(anId);
          if (node.style.display=="none")
          {
            // Contenu caché, le montrer
            node.style.display = "inline";
            node.style.height = "0";			// Optionnel rétablir la hauteur
          }
          else
          {
            // Contenu visible, le cacher
            node.style.display = "none";
            node.style.height = "0";			// Optionnel libérer l'espace
          }
            var img = new Image();
            img.src = "reccources/img6b.png";
            document.getElementById('body').backgroundImage = img.src;
        }

        toggle("donneesPointeuse");
        toggle("tablesBDD");
        toggle("Modifs");
        toggle('Users');
        toggle('Recompenses');

        function CheckClick(id){
        if(document.getElementById(id).checked){
        document.getElementById(id+" span").style.color='#1732E1';
        document.getElementById(id+" span").style.fontWeight='bold';
        }
        else {
          document.getElementById(id+" span").style.color='#D6DEE6';
          document.getElementById(id+" span").style.fontWeight='normal';
        }
        }

        </script>



  <?php }
  else {
    header('Location: index.php');
    exit();
  }

}
      else {
        header('Location: index.php');
        exit();
      } ?>
</html>
