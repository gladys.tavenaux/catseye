<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{
$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
$estChef = $sqlite->EstChef($_SESSION['id']);
$estAdmin = $sqlite->EstAdmin($_SESSION['id']);

?>

<style>
  body{
    background-image: url("ressources/img6b.jpg"), linear-gradient(#858686, #090909);
    background-repeat: no-repeat;
    background-size: cover;
  }
</style>



<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Dashboard</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body>
        <div class="container">
            <div class="page-header"></br>
                <h1>BASE DE DONNEES POINTEUSE</h1></br></br>
            </div>

            <h5>Y'aura les dashboards, promis, mais plus tard !</h5>
            <br><a href="pointeuse.php" class="lienPages">Retour</a>

        </div>
<?php }
      else {
        header('Location: index.php');
        exit();
      } ?>
    </body>
</html>
