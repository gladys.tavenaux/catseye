<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{

$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
// create new tables
//$sqlite->createTables(); Crée une vieille table de tutoriel (SQLiteCreateTable.php)
// get the table list
$tables = $sqlite->getTableList();
$shifts = $sqlite->getShifts($_SESSION['id']);
$employees = $sqlite->getEmployees();
$Equipe = $sqlite->getEquipe();
$shiftsOfEmployee = $sqlite->getShiftsForId($_SESSION["id"]);
$estChef = $sqlite->EstChef($_SESSION['id']);
$AgentByTeam = $sqlite->getAgentByTeam();
$ChefByTeam=$sqlite->getChefByTeam();

/*Actualise la page une fois les données modifiées*/
function actualise()
{
  ?><script>document.location.reload(false);document.location='AjouterUser.php'</script><?php
}
/*Fin de la fonction Actualise*/



/*Formulaire d'ajout d'un user*/
if(isset($_POST['Ajouter']) && isset($_POST['name']) && ($_POST['name']) != null) {
    $name = $_POST['name'];
    $mdp = $_POST['pass'];
    $confirmMdp = $_POST['pass2'];
    $nom = $_POST['nom'];
    $conform=false;
    $cara=false;
    $chiffre=false;
    $maj=false;
    $min=false;

    function verifidentifiant($texte){
      $lastring='&é"(-è_çà)=~#{[|`^@]}`]}"^$*ù!:;,¨£µ%§/.?¤<>';
      $rep=strpbrk ($texte, $lastring);
      if($rep==false){return false;}
      else{return true;}
    }

    $pseudoConf=verifidentifiant($name);
    $cara=verifidentifiant($mdp);

    for($i=0;$i<strlen($mdp);$i=$i+1){
      $c=$mdp[$i];
      if(is_numeric($c)){$chiffre=true;}
      else{
        if($c==strtoupper($c)){$maj=true;}
        if($c==strtolower($c)){$min=true;}
      }
    }

    $nb=0;
    if($cara==true){$nb=$nb+1;}
    if($chiffre==true){$nb=$nb+1;}
    if($maj==true){$nb=$nb+1;}
    if($min==true){$nb=$nb+1;}
    if($nb>=3){$conform=true;}

/*echo('cara'); echo($cara); echo('chiffre'); echo($chiffre); echo('maj'); echo($maj); echo('min'); echo($min); echo('conform'); echo($conform);*/

    if(strlen($name)<4 || $pseudoConf==true){
      echo '<script type="text/javascript">window.alert("Merci d\'entrer un pseudo d\'au moins 4 caractères sans caractère spécial.");</script>';
    }
    elseif (strlen($nom)<3 ) {
       echo '<script type="text/javascript">window.alert("Merci d\'entrer le nom complet.");</script>';
    }
    elseif (strlen($mdp)<8 || $conform==false) {
       echo '<script type="text/javascript">window.alert("Le mot de passe doit faire plus de 7 caractères, et contenir 3 de ces 4 spécificités : majuscule, minuscule, chiffre et un caractère spécial.");</script>';
    }
    elseif($mdp==$confirmMdp && $conform==true && $pseudoConf==false){
      $sqlite->ModifBDD('Ajout user','',$name.", ".$nom,$sqlite->getIdFromUsername($_SESSION['name']));
      $rep=$sqlite->CreateUser($name, $mdp);
      if($rep==true)
      {echo '<script type="text/javascript">window.alert("Utilisateur créé !");</script>'; Actualise();}
      else {
        echo '<script type="text/javascript">window.alert("Erreur lors de la création de l\'utilisateur..");</script>';
      }
    }
    elseif(isset($_POST['Ajouter'])) {
      echo '<script type="text/javascript">window.alert("Merci d\'entrer un pseudo");</script>';
    }
    else {
        echo '<script type="text/javascript">window.alert("Merci d\'entrer deux mots de passe identiques");</script>';
    }


}
/*Fin du formulaire d'ajout d'un user*/


/*Formulaire d'ajout ou suppression d'un agent dans une équipe*/
  if(isset($_POST['Changer']) && isset($_POST['username'])
  && isset($_POST['Nom_Equipe']) && isset($_POST['actionEquipe'])
  && $_POST['username'] != null && $_POST['actionEquipe'] != null
  && $_POST['Nom_Equipe'] != null) {
      $username = $_POST['username'];
      $act = $_POST['actionEquipe'];
      $NE = $_POST['Nom_Equipe'];
      $rep=$sqlite->ChangeEquipe($username, $act, $NE);

        if($rep>0){echo '<script type="text/javascript">window.alert("Equipe changée !");</script>';
          $sqlite->ModifBDD($act.' '.$NE,'',$username,$sqlite->getIdFromUsername($_SESSION['name']));
        actualise();}
        else {
          echo '<script type="text/javascript">window.alert("Erreur lors du changement d\'équipe..");</script>';
          echo '<script type="text/javascript">window.alert("'.$rep.'");</script>';
        }
  }
  elseif(isset($_POST['Changer'])){
    echo '<script type="text/javascript">window.alert("Merci de choisir une valeur par proposition.");</script>';
  }
/*Fin du formulaire d'ajout/suppression d'user dans une équipe*/

/*Formulaire d'ajout/suppression de chef par équipe*/
    if(isset($_POST['ChangerChef']) && isset($_POST['username'])
    && isset($_POST['Nom_Equipe']) && isset($_POST['actionEquipe'])
    && $_POST['username'] != null && $_POST['actionEquipe'] != null
    && $_POST['Nom_Equipe'] != null) {
        $username = $_POST['username'];
        $act = $_POST['actionEquipe'];
        $NE = $_POST['Nom_Equipe'];


          $rep=$sqlite->ChangeChef($username, $act, $NE);
          if($rep>0)
          {echo '<script type="text/javascript">window.alert("Chef changé !");</script>';
            $sqlite->ModifBDD($act.' '.$NE.' en chef','',$username,$sqlite->getIdFromUsername($_SESSION['name']));
          actualise();}
          else {
            echo '<script type="text/javascript">window.alert("Erreur lors du changement d\'équipe..");</script>';
            echo '<script type="text/javascript">window.alert("'.$rep.'");</script>';
          }

      }
      elseif(isset($_POST['ChangerChef']) && isset($_POST['username'])
      && isset($_POST['Nom_Equipe']) && isset($_POST['actionEquipe'])){
        echo '<script type="text/javascript">window.alert("Merci de choisir une valeur par proposition.");</script>';;
      }
/*Fin du formulaire d'ajout/suppression de chef par équipe*/

/*Formulaire de modification d'utilisateurs*/
    if(isset($_POST['ModifUser']) && isset($_POST['pseudo'])) {
        $user = $_POST['pseudo'];
        $id = $sqlite->getIdFromUsername($user);
        if($id<0){
          echo '<script>window.alert("Merci de sélectionner un utilisateur dans la liste")</script>';
        }

//actualise

        else{
          if(isset($_POST['fullname']) && strlen($_POST['fullname'])>5){
            $fullname = $_POST['fullname'];
            $old=$sqlite->getFullName($id);
            $sqlite->ModifFullName($id, $fullname);
            $sqlite->ModifBDD('Modif fullname of '.$user,$old,$fullname,$sqlite->getIdFromUsername($_SESSION['name']));
            actualise();
          }

          if(isset($_POST['newpseudo']) && strlen($_POST['newpseudo'])>5){
            $newpseudo = $_POST['newpseudo'];
            $sqlite->ModifPseudo($id, $newpseudo);
            $sqlite->ModifBDD('Modif pseudo of '.$user,$user,$newpseudo,$sqlite->getIdFromUsername($_SESSION['name']));
            actualise();
          }
        }
  }
/*Fin du formulaire de modification des utilisateurs*/


?>

<!--La déco-->


<script type="text/javascript">

/** Fonction basculant la visibilité d'un élément dom
* @parameter anId string l'identificateur de la cible à montrer, cacher
*/
function toggle(anId)
{
node = document.getElementById(anId);
if (node.style.visibility=="hidden")
{
// Contenu caché, le montrer
node.style.visibility = "visible";
node.style.height = "auto";			// Optionnel rétablir la hauteur
}
else
{
// Contenu visible, le cacher
node.style.visibility = "hidden";
node.style.height = "0";			// Optionnel libérer l'espace
}
}

</script>



<style> .monBody{
background-image: url("img6b.jpg"), linear-gradient(#858686, #090909);
}
</style>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="PELLEMOINE Martin">
      <title>Modifier les utilisateurs</title>
      <link href="monCSS.css" rel="stylesheet">
      <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">


  </head>

  <body class="monBody">
      <div class="container">
          <div class="page-header"></br>
              <h1>Gestion des utilisateurs</h1>

          <h4 style="text-align: center;"><?php echo "Pour l'équipe de l'utilisateur: " . $_SESSION["name"]; ?></h4>
        </div>

<table id="Rangement" style="border-collapse: separate; border-spacing: 2em 5em;">
  <tr><td colspan="2"><h2>Ajouter ou modifier un utilisateur</h2></td></tr>
  <tr><td rowspan="2">

<!--Affiche tous les users-->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="colonne">Username</th>
                        <th class="colonne">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($employees as $employee) : ?>
                        <tr>
                            <td class="case"><?php echo $employee['username'] ?></td>
                            <td class="case"><?php echo $employee['status'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
</td><td>
  <!--Ajout d'un user à la BDD avec un pseudo, fullname et un mdp crypté-->
  <div class="form">
      <fieldset style="width: 20em;">
      <form method="POST" action="AjouterUser.php">
      <table cellspacing="5" style="width:35em;">

        <tr><th style="width: 10em;"></th><th></th><th></th></tr>
    <tr><td><h5><u>Ajouter :</u></h5>

        <tr><td style="width:15em;">Pseudo :</td><td style="width:20em;"> <input type="text" name="name" autocomplete="off"></td>
          <td rowspan="4"><input type="submit" name="Ajouter" class="rond" autocomplete="off"></td></tr>
        <tr><td>Nom complet :</td><td> <input type="text" name="nom" autocomplete="off"></td></tr>
        <tr><td>Mot de passe :</td><td> <input type="password" name="pass" autocomplete="off"></td></tr>
        <tr><td>Confirmation :</td><td> <input type="password" name="pass2" autocomplete="off"></td></tr>


      </table></br>
      </form>
      </fieldset>
    </div>
      </td>
    </tr>
</td><td>
  <div class="form">
        <fieldset style="width: 20em;">
        <form method="POST" action="AjouterUser.php">
        <table cellspacing="5" style="width:35em;">
    <tr><td><h5><u>Modifier :</u></h5>

    <tr><td style="width:15em;">Pseudo :</td><td style="width:20em;">
      <input type=text list=users name="pseudo" autocomplete="off">
        <datalist id=users >
          <?php foreach ($employees as $employee) : ?>
              <option><?php echo $employee['username'] ?></option>
          <?php endforeach; ?>
        </datalist>
      </td>
      <td rowspan="3"><input type="submit" name="ModifUser" class="rond" autocomplete="off"></td></tr>
    <tr><td>Nouveau pseudo :</td><td> <input type="text" name="newpseudo" autocomplete="off"></td></tr>
    <tr><td>Nom complet :</td><td> <input type="text" name="fullname" autocomplete="off"></td></tr>


  </table></br>
  </form>
  </fieldset>
</div>
  </td>
</tr>


<tr><td colspan="2"><h2>Changer les équipes</h2></td></tr><tr><td>

<!--Affiche les employés et leur équipe-->
  <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="3" class="titre">Employé par équipe</th>
        </tr>
          <tr>
              <th class="colonne">ID</th>
              <th class="colonne">Pseudo</th>
              <th class="colonne">Equipe</th>
          </tr>
      </thead>
      <tbody>
          <?php foreach ($AgentByTeam as $employee) : ?>
              <tr>
                  <td class="case"><?php echo $employee['id'] ?></td>
                  <td class="case"><?php echo $employee['Pseudo'] ?></td>
                  <td class="case"><?php echo $employee['Equipe'] ?></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>

</td><td>
<!--Pour ajouter ou supprimer un utilisateur d'une équipe-->
<div class="form">
  <fieldset>
    <!--<select name="rep" id="rep" tabindex="3" class="zoneForm220" multiple>-->
    <form method="POST" action="AjouterUser.php">
      <table cellspacing="5">
        <tr><th></th><th style="width: 15em;"></th><th></th></tr>
    <tr><td>Utilisateur :</td><td>

      <input type=text list=users name="username">
    <datalist id=users >
      <?php foreach ($employees as $employee) : ?>
          <option><?php echo $employee['username'] ?></option>
      <?php endforeach; ?>
    </datalist>

  </td></tr>

  <tr><td>Action :</td><td>

    <input type=text list=actionEquipe name="actionEquipe">
    <datalist id=actionEquipe >
      <option>Ajouter à</option>
      <option>Supprimer de</option>
    </datalist></td><td colspan="3">

      <input type="submit" name="Changer" class="rond" autocomplete="off">

  </td></tr>

    <tr><td>Equipe :</td><td>

      <input type=text list=squads name="Nom_Equipe">
    <datalist id=squads >
      <?php foreach ($Equipe as $squad) : ?>
          <option autocomplete="off"><?php echo $squad['Nom_Equipe'] ?></option>
      <?php endforeach; ?>
    </datalist>

  </td></tr>
</table>
  </form>
  </fieldset>
</div>

</td></tr><tr><td colspan="2">
  <h2>Changer les chefs d'équipe</h2><br></td></tr><tr><td>
<!--Affiche les chefs et leur(s) équipe(s)-->
  <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="3" class="titre">Chef par équipe</th>
        </tr>
          <tr>
              <th class="colonne">ID</th>
              <th class="colonne">Chef</th>
              <th class="colonne">Equipe</th>
          </tr>
      </thead>
      <tbody>
          <?php foreach ($ChefByTeam as $employee) : ?>
              <tr>
                  <td class="case"><?php echo $employee['id'] ?></td>
                  <td class="case"><?php echo $employee['Chef'] ?></td>
                  <td class="case"><?php echo $employee['Equipe'] ?></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>


</td><td>
<div class="form">
  <!--Pour ajouter ou supprimer un chef pour une équipe-->
  <fieldset>
    <!--<select name="rep" id="rep" tabindex="3" class="zoneForm220" multiple>-->
    <form method="POST" action="AjouterUser.php">
      <table cellspacing="5">
        <tr><th></th><th style="width: 15em;"></th><th></th></tr>
        <tr>
          <td>Utilisateur :</td><td>
    <input type=text list=users name="username">
    <datalist id=users >
      <?php foreach ($employees as $employee) : ?>
          <option><?php echo $employee['username'] ?></option>
      <?php endforeach; ?>
    </datalist>
</td></tr><tr><td>Action :</td><td>
    <input type=text list=actionEquipe name="actionEquipe">
    <datalist id=actionEquipe >
      <option>Ajouter à </option>
      <option>Supprimer de </option>
    </datalist></td><td colspan="3">
      <input type="submit" name="ChangerChef" class="rond" autocomplete="off">
      </td></tr>
<tr><td>Equipe :</td><td>
    <input type=text list=squads name="Nom_Equipe">
    <datalist id=squads >
      <?php foreach ($Equipe as $squad) : ?>
          <option autocomplete="off"><?php echo $squad['Nom_Equipe'] ?></option>
      <?php endforeach; ?>
    </datalist>
  </td></th>
    </table>
  </form>
  </fieldset>
</div>
</td></tr></table>

            </br>

<a href="pointeuse.php" class='lienPages'>Retourner aux données</a>

  </div>
</br></br></br></br></br>

<?php }
else {
header('Location: index.php');
exit();
} ?>
  </body>
</html>
