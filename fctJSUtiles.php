<script type="text/javascript">
/** Fonction basculant la visibilité d'un élément dom
* @parameter anId string l'identificateur de la cible à montrer, cacher
*/
function toggle(anId)
{
node = document.getElementById(anId);
if (node.style.visibility=="hidden")
{
// Contenu caché, le montrer
node.style.visibility = "visible";
node.style.height = "auto";			// Optionnel rétablir la hauteur
}
else
{
// Contenu visible, le cacher
node.style.visibility = "hidden";
node.style.height = "0";			// Optionnel libérer l'espace
}
}

//Insertion, gestion des tableaux en js
function CouleursTableauJS(){
var arrayLignes = document.getElementById("monTableau").rows; //on récupère les lignes du tableau
var longueur = arrayLignes.length;//on peut donc appliquer la propriété length

for(var i=0; i<longueur; i++)//on peut directement définir la variable i dans la boucle
{
	var arrayColonnes = arrayLignes[i].cells;//on récupère les cellules de la ligne
	var largeur = arrayColonnes.length;
  numeroLigne(arrayLignes[i]);

	for(var j=0; j<largeur; j++)
	{
		if(j % 2 == 0)//si la clé est paire
		{
      if(i % 2 == 0)//si la clé est paire
    	{
    		arrayLignes[i].style.backgroundColor = "#bdcbf5";
    	}
      else //elle est impaire
  		{
  			arrayColonnes[j].style.backgroundColor = "#829eeb";
  		}
		}
		else //elle est impaire
		{
      if(i % 2 == 0)//si la clé est paire
    	{
    		arrayLignes[i].style.backgroundColor = "#bdcbf5";
    	}
      else //elle est impaire
  		{
  			arrayColonnes[j].style.backgroundColor = "#829eeb";
  		}
		}
	}
}
}


function numeroLigne(ligne)
{
	var numero = ligne.rowIndex;
	ligne.cells[0].innerHTML = numero+1;
}


function addRowHandlers() {
  CouleursTableauJS();
    var table = document.getElementById("monTableau");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler =
            function(row)
            {
                return function() {
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
                                        alert("id:" + id);
                                 };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}
window.onload = addRowHandlers();


function onRowClick(tableId, callback) {
    var table = document.getElementById(tableId),
        rows = table.getElementsByTagName("tr"),
        i;
    for (i = 0; i < rows.length; i++) {
        table.rows[i].onclick = function (row) {
            return function () {
                callback(row);
            };
        }(table.rows[i]);
    }
};

windows.onload = onRowClick("monTableau", function (row){
    var value = row.getElementsByTagName("td")[0].innerHTML;
    document.getElementById('click-response').innerHTML = value + " clicked!";
    console.log("value>>", value);
});
</script>
