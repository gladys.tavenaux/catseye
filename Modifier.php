<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;




if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{
$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());

  $estChef = $sqlite->EstChef($_SESSION['id']);
  $estAdmin = $sqlite->EstAdmin($_SESSION['id']);

  if($estChef==true || $estAdmin==true){

// create new tables
//$sqlite->createTables(); Crée une vieille table de tutoriel (SQLiteCreateTable.php)
// get the table list
$tables = $sqlite->getTableList();
$shifts = $sqlite->getShifts($_SESSION['id']);
$employees = $sqlite->getEmployees();
$shiftsOfEmployee = $sqlite->getShiftsForId($_SESSION["id"]);
$allShifts = $sqlite->getAllShifts();


if(isset($_POST['envoyer'])){
  if ($_POST['envoyer']=='perso'){
    $result=$shiftsOfEmployee;
  }
  elseif ($_POST['envoyer']=='team') {
    if($estChef==true){
      $result=$shifts;
    }
  }
  elseif ($_POST['envoyer']=='admin') {
    if($estAdmin==true){
      $result=$allShifts;
    }
  }
  else {
    $result=$shiftsOfEmployee;
  }

  $fp = fopen('file.csv', 'w');

  foreach ($result as $fields) {
      $slite->fputcsv2($fp, $fields);
  }



$file = 'file.csv';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    fclose($fp);
    exit;
}
else{  echo '<script type="text/javascript">window.alert("Erreur");</script>';}

}


/*                         Transfère en PDF                           */
if(isset($_POST['pdf'])){
require('app/fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

if ($_POST['pdf']=='perso'){
  $result=$shiftsOfEmployee;

  $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
  $pdf->Cell(30,10,'',0,1,'C');

    $pdf->Cell(25,10,'Date',1);
    $pdf->Cell(35,10,'Debut de shift',1);
    $pdf->Cell(35,10,'Fin de shift',1);
    $pdf->Cell(30,10,'Duree du shift',1);
    $pdf->Cell(30,10,'Temp de pause',1);
    $pdf->Cell(35,10,'Temp de formation',1,1,'C');

    $pdf->SetFont('Arial','',9);

  foreach ($result as $row) {
    $pdf->Cell(25,10,$row['date'],1);
    $pdf->Cell(35,10,$row['debut'],1);
    $pdf->Cell(35,10,$row['fin'],1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
    $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
  }
}
elseif ($_POST['pdf']=='team') {
  if($estChef==true){$result=$shifts;
$pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'equipe par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
elseif ($_POST['pdf']=='admin') {
  if($estAdmin==true){
    $result=$allShifts;
    $pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'admin par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
  else {
    $result=$shiftsOfEmployee;

    $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');

      $pdf->Cell(25,10,'Date',1);
      $pdf->Cell(35,10,'Debut de shift',1);
      $pdf->Cell(35,10,'Fin de shift',1);
      $pdf->Cell(30,10,'Duree du shift',1);
      $pdf->Cell(30,10,'Temp de pause',1);
      $pdf->Cell(35,10,'Temp de formation',1,1,'C');

      $pdf->SetFont('Arial','',9);

    foreach ($result as $row) {
      $pdf->Cell(25,10,$row['date'],1);
      $pdf->Cell(35,10,$row['debut'],1);
      $pdf->Cell(35,10,$row['fin'],1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
      $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
    }
  }

$pdf->Output();
}

if(isset($_POST['Modifier'])){

  if(!isset($_POST['id']) || $_POST['id']==null){
     echo '<script type="text/javascript">window.alert("Merci de spécifier l\'id du shift à modifier");</script>';
  }
  elseif (!isset($_POST['champ']) || $_POST['champ']==null) {
    echo '<script type="text/javascript">window.alert("Merci de spécifier le champ du shift à modifier");</script>';
  }
  elseif (!isset($_POST['valeur']) || $_POST['valeur']==null) {
    echo '<script type="text/javascript">window.alert("Merci de spécifier la valeur à modifier");</script>';
  }
  else{
    $shiftid=$_POST['id'];
    $champ=$_POST['champ'];
    $valeur=$_POST['valeur'];
    $vrai=false;

    $vrai=is_numeric($id);
    if(!$vrai){echo '<script type="text/javascript">window.alert("L\'id est un chiffre");</script>';}
    elseif($champ=="Pseudo"){
      $champ="employee_id";
    }
    elseif($champ=="Début"){
      $champ="debut";
    }
    elseif($champ=="Fin"){
      $champ="fin";
    }
    elseif($champ=="Temps de pause"){
      $champ="pause_duration";
    }
    elseif($champ=="Formation QA"){
      $champ="qa_formation_duration";
    }
    else {echo '<script type="text/javascript">window.alert("Merci de choisir le champ à modifier depuis le menu déroulant.");</script>'; $vrai=false;}

    if($vrai==true){


      $sql = 'UPDATE shift SET :champ = :valeur WHERE id = :id;';

      $stmt = $this->pdo->prepare($sql);
              $stmt->bindValue(':id', $id);
              $stmt->bindValue(':champ', $champ);
              $stmt->bindValue(':valeur', $valeur);
              return $stmt->execute();
    }

  }
}
?>

<script type="text/javascript">

</script>

<style> .monBody{
background-image: url("img6b.jpg"), linear-gradient(#858686, #090909);
}
</style>



<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="PELLEMOINE Martin">
        <title>Modifier les données</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body class="monBody">
        <div class="container">
            <div class="page-header"></br>
                <h1>Modifier les données de la pointeuse</h1></br></br>
            </div>



<?php
if($estChef==true){?><br><br>
            <h2><?php echo "Pour l'équipe de l'utilisateur: " . $_SESSION["name"]; ?></h2><br>

<table class="no-border"><tr><td style="width: 25em;">
            <form action="pointeuse.php" method="POST">
              <input type="hidden" name="envoyer" value="team">
              <input type="submit" value="Télécharger ces données en CSV">
            </form></td>
            <td><form action="pointeuse.php" method="POST">
              <input type="hidden" name="pdf" value="team">
              <input type="submit" value="Télécharger ces données en PDF">
            </form></td></tr></table>

<div id="jsContainer">
            <table class="table table-bordered" id="monTableau">
                <thead>
                    <tr>
                      <th class="colonneId">id</th>
                        <th class="colonne">Agent</th>
                        <th class="colonne">Id Agent</th>
                        <th class="colonne">Date shift</th>
                        <th class="colonne">Début du shift</th>
                        <th class="colonne">Fin du shift</th>
                        <th class="colonne">Durée du shift</th>
                        <th class="colonne">Durée des pauses</th>
                        <th class="colonne">Durée de la formation QA</th>
                    </tr>
                </thead>
                <tbody style="cursor:pointer">
                  <?php foreach ($shifts as $shift) : ?>
                        <tr>
                          <td class="caseid"><?php echo $shift['shiftid'] ?></td>
                            <td class="case"><?php echo $shift['Pseudo'] ?></td>
                            <td class="case"><?php echo $shift['id'] ?></td>
                            <td class="case"><?php echo $shift['date'] ?></td>
                            <td class="case"><?php echo $shift['debut'] ?></td>
                            <td class="case"><?php echo $shift['fin'] ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['durée']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['temps de pause']) ?></td>
                            <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['Formation QA']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>

<div style="width: 40em; border: 1px solid #000000; background: radial-gradient(#666666,#232223); border-radius: 30px; color: #232223;">
          <fieldset style="margin-left:1em;">
            <legend style="color: white; text-shadow: grey;">Modifier un shift</legend>
            <form method="POST" action="Modifier.php">
              <table cellspacing="5" style="width:35em; color: #B3B3B3;">
                <tr><th style="width: 10em;"></th><th></th><th></th></tr>
            <tr><td>

                <tr><td style="width:15em;">id du shift :</td><td style="width:20em;"> <input type="text" name="id" autocomplete="off"></td>
                  <td rowspan="3"><input type="submit" name="Modifier" autocomplete="off"></td></tr>
                <tr><td>Champ :</td><td>
                            <input type=text list=champ name="champ">
                            <datalist id=champ>
                            <option>Pseudo</option>
                            <option>Début</option>
                            <option>Fin</option>
                            <option>Temps de pause</option>
                            <option>Formation QA</option>
                            </datalist></td><td colspan="3"></td></tr>
                <tr><td>Valeur :</td><td> <input type="text" name="valeur" autocomplete="off"></td></tr>


              </table></br>
            </form>
          </fieldset>
</div>
<br>
          <a href="AjouterUser.php" class="lienPages">Ajouter un utilisateur</a> <a href="pointeuse.php" style="margin-left:3em;" class="lienPages">Retour aux données</a>

        <?php }
        elseif ($estAdmin==true){ ?>
              <table class="no-border"><tr><td style="width: 25em;">
                      <form action="pointeuse.php" method="POST">
                        <input type="hidden" name="envoyer" value="admin">
                        <input type="submit" value="Télécharger ces données en CSV">
                      </form></td>
                      <td><form action="pointeuse.php" method="POST">
                        <input type="hidden" name="pdf" value="admin">
                        <input type="submit" value="Télécharger ces données en PDF">
                      </form></td></tr></table>
<div id="jsContainer">
                      <table class="table table-bordered">
                          <thead>
                              <tr>
                                <th class="colonneId">id</th>
                                  <th class="colonne">Agent</th>
                                  <th class="colonne">Id agent</th>
                                  <th class="colonne">Date shift</th>
                                  <th class="colonne">Début du shift</th>
                                  <th class="colonne">Fin du shift</th>
                                  <th class="colonne">Durée du shift</th>
                                  <th class="colonne">Durée des pauses</th>
                                  <th class="colonne">Durée de la formation QA</th>
                              </tr>
                          </thead>
                          <tbody>
                                  <tr>
                                    <?php foreach ($allShifts as $shift) : ?>
                                      <td class="caseid"><?php echo $shift['shiftid'] ?></td>
                                      <td class="case"><?php echo $shift['Pseudo'] ?></td>
                                      <td class="case"><?php echo $shift['id'] ?></td>
                                      <td class="case"><?php echo $shift['date'] ?></td>
                                      <td class="case"><?php echo $shift['debut'] ?></td>
                                      <td class="case"><?php echo $shift['fin'] ?></td>
                                      <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['durée']) ?></td>
                                      <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['temps de pause']) ?></td>
                                      <td class="case"><?php echo $sqlite->getReadableTimeWithSeconds($shift['Formation QA']) ?></td>
                                  </tr>
                              <?php endforeach; ?>
                          </tbody>
                      </table> </div>


                      <div style="width: 40em; border: 1px solid #000000; background: radial-gradient(#666666,#232223); border-radius: 30px; color: #232223;">
                                <fieldset style="margin-left:1em;">
                                  <legend style="color: white; text-shadow: grey;">Modifier un shift</legend>
                                  <form method="POST" action="Modifier.php">
                                    <table cellspacing="5" style="width:35em; color: #B3B3B3;">
                                      <tr><th style="width: 10em;"></th><th></th><th></th></tr>
                                  <tr><td>

                                      <tr><td style="width:15em;">id du shift :</td><td style="width:20em;"> <input type="text" name="id" autocomplete="off"></td>
                                        <td rowspan="3"><input type="submit" name="Modifier" class="rond" autocomplete="off"></td></tr>
                                      <tr><td>Champ :</td><td>
                                                  <input type=text list=champ name="champ">
                                                  <datalist id=champ>
                                                  <option>Pseudo</option>
                                                  <option>Début</option>
                                                  <option>Fin</option>
                                                  <option>Temps de pause</option>
                                                  <option>Formation QA</option>
                                                  </datalist></td><td colspan="3"></td></tr>
                                      <tr><td>Valeur :</td><td> <input type="text" name="valeur" autocomplete="off"></td></tr>


                                    </table></br>
                                  </form>
                                </fieldset>
                      </div>

        <a href="AjouterUser.php" class="lienPages">Ajouter un utilisateur</a> <a href="pointeuse.php" style="margin-left:3em;" class="lienPages">Retour</a>
        <?php } ?>
        </div>
        <br><br>

      <?php }
      else {
        header('Location: pointeuse.php');
        exit();
      }
    }
    else {
      header('Location: index.php');
      exit();
    } ?>
    </body>
</html>
