<html>
<head>
  <title>Pointeuse - Connexion</title>
</head>
  <style>
    body.accueil{
      margin-left: 40%;
      margin-top : 15%;
      background-image: url("ressources/img5.jpg"), linear-gradient(#858686, #090909);
      background-repeat: no-repeat;
      background-size: 100%;
      align-items: center;
    }
.rond{width:85px;
 height:60px;
 background:radial-gradient(#FFBAFD,#F195F9);
 font:bold 13px Arial;
 border-radius:100%;
 border-color: #FFBAFD;
 color:#422744;
 box-shadow: 5px 5px 5px #5D5D5D;
 margin-top: 2em;
 margin-left: 7em;
}

.rond:hover{
background:radial-gradient(#FE7FFC,#FCD3FF);

box-shadow: 5px 5px 5px #CCCCCC;
}

.colonne{
  width:85px;
  height:25px;
background: linear-gradient(#181618,#544D53);
color:#FFFFFF;
text-align: center;
border:1px solid #000000;
text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
border-color: #000000;
border-radius:10%;
}
.titre{
  width:120px;
  height:30px;
background: linear-gradient(#000000,#1E1E1E);
color: #FFFFFF;
text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
border-width: thick;
border-radius:10%;
border-color: #000000;
}
fieldset{
  background-image: url("ressources/img5b.jpg"), linear-gradient(#858686, #090909);
  background-repeat: no-repeat;
  background-size: cover;

  border:2px solid #000000;
    -moz-border-radius:8px;
    -webkit-border-radius:8px;
    border-radius:8px;
}
  </style>

<?php
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());


if(isset($_POST['valider'])&&($_POST['pass']!=null)) {

  if(isset($_POST['name'])&&$_POST['name']!='') {
    $name = $_POST['name'];
    $id = $sqlite->getIdFromUsername($name);
      $mdp = $_POST['pass'];
      $ResultatMdp=$sqlite->CompareMDP($mdp, $id);

  if ($id == -1){
    echo '<script type="text/javascript">window.alert("Identifiants incorrects");</script>';
  }
elseif ($ResultatMdp==false) {
  echo '<script type="text/javascript">window.alert("Identifiants incorrects");</script>';
}
elseif ($ResultatMdp==true&&$id>0) {
session_start();
$_SESSION['name']=$_POST['name'];
$_SESSION['id']=$id;
header("Location: pointeuse.php".SID);
}
else {
  echo '<script type="text/javascript">window.alert("Erreur lors de la recherche de l\'utilisateur.");</script>';
}
  }
  else {
      echo '<script type="text/javascript">window.alert("Erreur lors de la recherche de l\'utilisateur.");</script>';
  }
}
elseif(isset($_POST['valider'])) {
  echo '<script type="text/javascript">window.alert("Merci d\'entrer vos informations de connexion.");</script>';
}

 ?>

<body class="accueil">
  <fieldset style="width: 15em;">
<legend><h2 class="titre">Connexion</h2></legend>
<form action="index.php" method="POST">
  <table>
<tr><td class="colonne">Name:</td><td> <input type="text" name="name" autocomplete="off"></td></tr>
<tr><td class="colonne">Password:</td><td> <input type="password" name="pass" autocomplete="off"></td></tr>
<tr><td colspan="2"><input type="submit" name="valider" class="rond"></td></tr>
</table>
</form>
</fieldset>
<br>
</body>
</html>
