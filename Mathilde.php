<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;
use App\PHP4Paie as Paie;

if(isset($_SESSION['id']) && $_SESSION['id'] != null){


$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
$paie =  new Paie((new SQLiteConnection())->connect());

$tables = $sqlite->getTableList();
$employees = $sqlite->getEmployees();
$allShifts = $sqlite->getAllShifts();
$estAdmin = $sqlite->EstAdmin($_SESSION['id']);
$modifs=$sqlite->getModifications();
$users=$sqlite->getAllAboutUsers();


if($estAdmin==true) {


if(isset($_POST['envoyer'])){
  if ($_POST['envoyer']=='perso'){
    $result=$shiftsOfEmployee;
  }
  elseif ($_POST['envoyer']=='team') {
    if($estChef==true){
      $result=$shifts;
    }
  }
  elseif ($_POST['envoyer']=='admin') {
    if($estAdmin==true){
      $result=$allShifts;
    }
  }
  else {
    $result=$shiftsOfEmployee;
  }

  $fp = fopen('file.csv', 'w');

  foreach ($result as $fields) {
      fputcsv2($fp, $fields);
  }



$file = 'file.csv';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    fclose($fp);
    exit;
}
else{  echo '<script type="text/javascript">window.alert("Erreur");</script>';}

}


/*                         Transfère en PDF                           */
if(isset($_POST['pdf'])){
require('app/fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

if ($_POST['pdf']=='perso'){
  $result=$shiftsOfEmployee;

  $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
  $pdf->Cell(30,10,'',0,1,'C');

    $pdf->Cell(25,10,'Date',1);
    $pdf->Cell(35,10,'Debut de shift',1);
    $pdf->Cell(35,10,'Fin de shift',1);
    $pdf->Cell(30,10,'Duree du shift',1);
    $pdf->Cell(30,10,'Temp de pause',1);
    $pdf->Cell(35,10,'Temp de formation',1,1,'C');

    $pdf->SetFont('Arial','',9);

  foreach ($result as $row) {
    $pdf->Cell(25,10,$row['date'],1);
    $pdf->Cell(35,10,$row['debut'],1);
    $pdf->Cell(35,10,$row['fin'],1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
    $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
    $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
  }
}
elseif ($_POST['pdf']=='team') {
  if($estChef==true){$result=$shifts;
$pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'equipe par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
elseif ($_POST['pdf']=='admin') {
  if($estAdmin==true){
    $result=$allShifts;
    $pdf->SetFont('Arial','B',8);

    $pdf->Cell(190,10,'Donnees pointeuse d\'admin par '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');


  $pdf->Cell(30,10,'Pseudo',1);
  $pdf->Cell(5,10,'id',1);
  $pdf->Cell(20,10,'Date',1);
  $pdf->Cell(30,10,'Debut de shift',1);
  $pdf->Cell(30,10,'Fin de shift',1);
  $pdf->Cell(22,10,'Duree du shift',1);
  $pdf->Cell(23,10,'Temp de pause',1);
  $pdf->Cell(30,10,'Temp de formation',1,1,'C');

  $pdf->SetFont('Arial','',8);
    foreach ($result as $row) {
      $pdf->Cell(30,10,$row['Pseudo'],1);
      $pdf->Cell(5,10,$row['id'],1);
      $pdf->Cell(20,10,$row['date'],1);
      $pdf->Cell(30,10,$row['debut'],1);
      $pdf->Cell(30,10,$row['fin'],1);
      $pdf->Cell(22,10,$sqlite->getReadableTimeWithSeconds($row['durée']),1);
      $pdf->Cell(23,10,$sqlite->getReadableTimeWithSeconds($row['temps de pause']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['Formation QA']),1,1,'C');
    }
  }
}
  else {
    $result=$shiftsOfEmployee;

    $pdf->Cell(190,10,'Donnees pointeuse de '.$_SESSION['name'],1,1,'C');
    $pdf->Cell(30,10,'',0,1,'C');

      $pdf->Cell(25,10,'Date',1);
      $pdf->Cell(35,10,'Debut de shift',1);
      $pdf->Cell(35,10,'Fin de shift',1);
      $pdf->Cell(30,10,'Duree du shift',1);
      $pdf->Cell(30,10,'Temp de pause',1);
      $pdf->Cell(35,10,'Temp de formation',1,1,'C');

      $pdf->SetFont('Arial','',9);

    foreach ($result as $row) {
      $pdf->Cell(25,10,$row['date'],1);
      $pdf->Cell(35,10,$row['debut'],1);
      $pdf->Cell(35,10,$row['fin'],1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['shift_duration']),1);
      $pdf->Cell(30,10,$sqlite->getReadableTimeWithSeconds($row['pause_duration']),1);
      $pdf->Cell(35,10,$sqlite->getReadableTimeWithSeconds($row['qa_formation_duration']),1,1,'C');
    }
  }

$pdf->Output();
}

if(isset($_POST['preference']) && $_POST['preference'] != null){ //name $i (0 à 13) et value = id de l'user. Envoyé si checked
  $tableau=[];
   foreach ($users as $shift) {
      for ($i=0; $i < 14 ; $i++) {
        if(isset($_POST[$i.$shift['id']])){
          $tableau[]=[$i, $_POST[$i.$shift['id']]];
          //echo '<script>window.alert("Réponse : '.implode(", ",$tableau[$i]).'")</script>';
        }
      }
    }

  foreach ($tableau as $row => $value) {
  /*  echo '<script>window.alert("Réponse : '.$row.', '.$tableau[$row][1].'")</script>';*///Date, i(shift), value(couleur), id
    /*echo '<script>window.alert("Réponse : '.*/
    $sqlite->InsertPreference($DateLundiProchain, $tableau[$row][0], 'blue', $tableau[$row][1])/*.'")</script>'*/;
  }
}



$CeLundi = date('d/m/Y', strtotime('last monday'));
$DateLundiProchain =date('d/m/Y',strtotime('next monday'));
$DateDebutShifts = date('d/m/Y',strtotime('14 january 2019'));


function SemaineSuivante($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

function SemainePrecedente($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)-($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

if(!isset($DateLundi) || $DateLundi == null)
{$DateLundi = $CeLundi;}

if (isset($_POST['ChoixSemaine']) && $_POST['ChoixSemaine'] !=null){
  $DateLundiProchain = $_POST['semaine'];
  $DateLundi = SemainePrecedente($DateLundiProchain);
}
else{$DateLundiProchain=SemaineSuivante($DateLundi);}
?>

<style>
  body{
    background-image: url("ressources/img6b.jpg");
    background-repeat: no-repeat;
    background-size: cover;
    margin: 0;
    padding: 0;
    border: none;
  }
  div{
    margin: 0;
    padding: 0;
  }

</style>






<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Gestion du support</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body>
        <div class="container">
            <div class="page-header"></br>
                <h1>Gestion du projet</h1></br></br>
            </div>

            <h2><?php echo "Pour l'utilisateur: " . $_SESSION["name"] ?></h2>


<div id=GestionShifts>
<br/>
<h3>Gestion des shifts</h3>
<form method="post" style="width: 15em; margin-left: 2em;">
  <h5>Choix de la semaine</h5>
  <input type=text list=lundis name="semaine" autocomplete="off">
  <datalist id=lundis >
    <?php
    $DateLundiProchain=SemaineSuivante($DateLundiProchain);
      for($BoucleLundi=$DateDebutShifts;
          strtotime($BoucleLundi) != strtotime(SemaineSuivante($DateLundiProchain));
          $BoucleLundi=SemaineSuivante($BoucleLundi)) {
        echo "<option>".$BoucleLundi."</option>";
      }
    ?>
  </datalist>
  <input type="submit" name="ChoixSemaine">
  </form>

<h5>Semaine du <?php echo $DateLundi;?></h5>
<table class="shifts">
  <tr>
    <td colspan="2">Utilisateur :</td>
    <?php foreach ($users as $shift) : ?>
    <td style="padding: 0 0.7em 0 0.7em;"><?php echo $shift['username'] ?></td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td rowspan="2">Lundi</td>
    <td>matin</td>

    <?php foreach ($users as $shift) : ?>
    <!-- <td><?php //echo $shift['id'] ?></td>-->
      <td rowspan="14">
        <table class="inshifts">
          <?php for ($i=0; $i < 14; $i++) {?>
            <tr><td style="color: <?php echo $sqlite->ColorInshiftsOf($DateLundi, $i, $shift['id']);?>; font-weight: bold; font-size: 15px;"><?php echo $sqlite->RepToInshifts($DateLundi, $i, $shift['id']);?></td></tr><!--il faut 14 lignes-->
          <?php } ?>
        </table>
      </td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mardi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mercredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Jeudi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Vendredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Samedi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Dimanche</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
</table> <br>
<span style="float: right; padding: 0;">
  <div class="form" style="width: auto">
<fieldset>
  <legend>
      <p><u>Légende :</u></p>
      <p><span style="color: blue">X</span> : Shift définitif</p>
      <p><span style="color: yellow">X</span> : Repos</p>
      <p><span style="color: green">X</span> : Shift souhaité</p>
      <p><span style="color: red">X</span> : Shift non souhaité</p>
      <p><span style="color: black">X</span> : Congés</p>
    </legend>
</fieldset>
</div>
</span>
<h5>Semaine du <?php echo $DateLundiProchain;?></h5>
<table class="shifts">
  <tr>
    <td colspan="2">Utilisateur :</td>
    <?php foreach ($users as $shift) : ?>
    <td style="padding: 0 0.7em 0 0.7em;"><?php echo $shift['username'] ?></td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td rowspan="2">Lundi</td>
    <td>matin</td>

    <?php foreach ($users as $shift) : ?>
    <!-- <td><?php //echo $shift['id'] ?></td>-->
      <td rowspan="14">
        <table class="inshifts">
          <?php for ($i=0; $i < 14; $i++) {?>
            <tr><td style="color: <?php echo $sqlite->ColorInshiftsOf($DateLundiProchain, $i, $shift['id']);?>; font-weight: bold; font-size: 15px;"><?php echo $sqlite->RepToInshifts($DateLundiProchain, $i, $shift['id']);?></td></tr><!--il faut 14 lignes-->
          <?php } ?>
        </table>
      </td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mardi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mercredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Jeudi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Vendredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Samedi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Dimanche</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
</table>



<br>
<table class='no-border'><tr><td>
<table class="shifts">
<h5>Attribution des shifts</h5>
  <form method="POST" action="Admin.php">
  <h6>Semaine du <?php echo $DateLundiProchain;?></h6>
  <tr>
    <td colspan="2">Utilisateur :</td>
    <?php foreach ($users as $shift) : ?>
    <td style="padding: 0 0.7em 0 0.7em;"><?php echo $shift['username'] ?></td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td rowspan="2">Lundi</td>
    <td>matin</td>

    <?php foreach ($users as $shift) : ?>
      <td rowspan="14">
        <table class="inshifts">
          <?php for ($i=0; $i < 14; $i++) {?>
            <tr><td>
            <input type="checkbox" name="<?php echo $i.$shift['id']; ?>" id="<?php echo $i." ".$shift['id']." xa"; ?>"
            value="<?php echo $shift['id']; ?>" onclick="CheckClick(this.id)" style="text-align: center;">
            <span id="<?php echo $i." ".$shift['id']." x spana"; ?>">S</span></input></td>
            <?php if($i%2==0){ ?>
              <td rowspan="2">
            <input type="checkbox" name="<?php echo $i.$shift['id']." repos"; ?>" id="<?php echo $i." ".$shift['id']." xb"; ?>"
            value="<?php echo $shift['id']." repos"; ?>" onclick="CheckClick(this.id)" style="text-align: center;">
            <span id="<?php echo $i." ".$shift['id']." x spanb"; ?>">R</span></input></td> <?php }?>
          </td></tr>
          <?php } ?>
        </table>
      </td>
    <?php endforeach; ?>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mardi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Mercredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Jeudi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Vendredi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Samedi</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
  <tr>
    <td rowspan="2">Dimanche</td>
    <td>matin</td>
  </tr>
  <tr>
    <td>aprem</td>
  </tr>
</table>
</td><td><input type="submit" name="preference" class="rond" autocomplete="off"></td></tr></table>
</form>
<br>
<?php
  if(isset($_POST['Supp'])) {
    $day = $_POST['DateSupp'];
    $shift=$_POST['ShiftSupp'];
    $id=$sqlite->getIdFromUsername($_POST['Agent'], $sqlite->getAllAboutUsers());
    $old=$sqlite->getThisPreference($day, $shift, $id);
    $idModif = $sqlite->ModifBDD("Delete preference $day $shift $id", $old, "", $_SESSION['id'], "preference");
    //echo"idModif".$idModif;
    if($idModif>0){
      $rep=$sqlite->DeletePreference($day, $shift, $id);
      if($rep>0){
        echo"<script>window.alert('OK !');</script>";
      }
      elseif($rep==""){
        echo"<script>window.alert('Ligne non trouvée.');</script>";
      }
      else {echo('<script>window.alert("Ouups, erreur !\\nDeletePreference<0 or false");</script>');
        $sqlite->AnnuleModif($idModif);}
    }
    else{echo('<script>window.alert("Youuups, erreur !\\nModifBDD<0");</script>');}
  }
 ?>

<div class="form" style="padding: 1em 0 1em 2em;">
<table class="borderLess" style="margin-left: 2em;">
  <form method='POST'>
    <h5>Supprimer une valeur</h5>
    <tr>
      <td>Jour : <br><i style="font-size: 12px; margin-right: 1em;">(format AAAA-MM-JJ)</i></td>
      <td><input type="text" name="DateSupp" autocomplete="off"></td>
      <td rowspan="3"><input type="submit" name="Supp" style="margin-left: 2em;"></td>
    </tr>
    <tr>
      <td>Shift :</td>
      <td> <input type=text list=shifts name="ShiftSupp" autocomplete="off">
        <datalist id=shifts >
          <option>matin</option>
          <option>aprem</option>
        </datalist>
      </td>
    </tr>
    <tr>
      <td>Agent :</td>
      <td><input type="text" list="Agents" name="Agent" autocomplete="off">
        <datalist id="Agents">
          <?php foreach($employees as $employee){
            echo "<option>".$employee['username']."</option>";
          }?>
      </td>
    </tr>
  </form>
</table>
</div>

</div id=GestionShifts>
<br/>


<div id=Paies style="margin:0; padding:0;"><br>
  <div style="border: 2px solid red; background-color: rgba(166, 12, 1, 0.3); padding-left:1em;">
    <p style="margin-left: 2em;"><u>Rappel :</u></p>
    <p>- les éléments à communiquer sont les éléments réels, et non les éléments planifiés ;</p>
    <p>- les éléments qui ont le caractère de rectification et qui ont trait aux mois passés sont à inscrire dans la colonne "Autre"</p>
  </div>
<br>
<h3>Gestion des paies</h3>
<?php
if (isset($_POST['ChoixMois']) && $_POST['ChoixMois'] !=null && isset($_POST['mois'])){
  $MoisChoisi=strtotime($_POST['mois']);
}
else{$MoisChoisi=strtotime("this month");}



  $moisDepart = strtotime("january 2019");
  $moisSuivant = strtotime("next month");

  function MoisSuivant($mois){
    return strtotime('+1 month',$mois);
  }
  ?>

  <form method="post" style="width: 15em; margin-left: 2em; margin-top: 1em;">
  <h5>Choix du mois</h5>
  <input type=text list=mois name="mois" autocomplete="off">
  <datalist id=mois autocomplete="off">
    <?php
      for ($moisBoucle=$moisDepart; $moisBoucle<$moisSuivant; $moisBoucle=MoisSuivant($moisBoucle)) {
        echo"<option>".date("Y-m",$moisBoucle)."</option>";
      }
    ?>
  </datalist>
  <input type="submit" name="ChoixMois">
  </form>



  <br><h5>Mois choisi : <?php echo date("Y-m",$MoisChoisi); ?> </h5>
<?php //$dates=$paie->GetDatesDansMois("2019-01-25","2019-02-05",$MoisChoisi);
      //foreach($dates as $row){echo $row['date']." ";}?>
  <table class="paies" style="margin:0;">

    <tr><td>Agent</td>
          <?php foreach ($users as $key) {
              echo("<td class='inpaie'>".$key['username']."</td>");
            }?>
    </tr>

    <tr><td>Nom complet</td>
          <?php foreach ($users as $key) {
              echo("<td class='inpaie'>".$key['fullname']."</td>");
            }?>
    </tr>

    <tr><td>Nombre de dimanches travaillés</td>
          <?php foreach ($users as $key) {
              echo("<td class='inpaie'>".$paie->GetNbDimanche($key['id'],$MoisChoisi)."</td>");
            }?>
    </tr>

    <tr><td>Nombre de jours fériés travaillés</td>
          <?php foreach ($users as $key) {
              echo("<td class='inpaie'>".$paie->GetNbJF($key['id'],$MoisChoisi)."</td>");
            }?>
    </tr>

    <tr><td>Date des jours fériés travaillés</td>
          <?php foreach ($users as $key) {
            echo("<td class='inpaie'>");
            $tab = $paie->GetJF($key['id'],$MoisChoisi);
            foreach($tab as $row){
              echo("<p>");
              echo ($row['date']);
              echo("<p>");
            }
            echo "</td>";
            }?>
    </tr>

        <tr><td>Jours de repos hebdomadaires sur la/les semaines incluant un jour férié travaillé</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Nombre d'heures travaillées de nuit hors dimanche, jour férié et heures supplémentaires</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Nombres d'heures supplémentaires travaillées</td>
              <?php foreach ($users as $key) {
                $nombre=0;
                echo("<td class='inpaie'>");
                $tab = $paie->GetHS($key['id'], $MoisChoisi);
                if(sizeof($tab)==0){echo("0");}
                else{
                  foreach($tab as $row){
                    if($row['nb']==""){echo ("0");}
                    $nombre=$nombre+$row['nb'];
                  }
                  echo $nombre;
                }
                echo "</td>";
                }?>
        </tr>

        <tr><td>Justification des heures supplémentaires</td>
              <?php foreach ($users as $key) {
                echo("<td class='inpaie'>");
                $tab = $paie->GetHS($key['id'], $MoisChoisi);
                foreach($tab as $row){
                  if($row['justif']!=""){
                    echo("<p>");
                    echo ($row['justif']);
                    echo("</p>");}
                }
                echo "</td>";
                }?>
        </tr>

        <tr><td>Personne ayant demandé les heures supplémentaires</td>
              <?php foreach ($users as $key) {
                echo("<td class='inpaie'>");
                $tab = $paie->GetHS($key['id'], $MoisChoisi);
                foreach($tab as $row){
                  if($row['demande']!=""){
                    echo("<p>");
                    echo ($row['demande']);
                    echo("</p>");}
                }
                echo "</td>";
                }?>
        </tr>

        <tr><td>Personne ayant autorisé les heures supplémentaires</td>
              <?php foreach ($users as $key) {
                echo("<td class='inpaie'>");
                $tab = $paie->GetHS($key['id'], $MoisChoisi);
                foreach($tab as $row){
                  if($row['autorise']!=""){
                    echo("<p>");
                    echo ($row['autorise']);
                    echo("</p>");}
                }
                echo "</td>";
                }?>
        </tr>

        <tr><td>Date des jours de congés pris</td>
              <?php foreach ($users as $key) {
                echo("<td class='inpaie'>");
                  $CP=$paie->GetCP($key['id'], $MoisChoisi);
                  if(sizeof($CP)!=0){
                  foreach($CP as $row){
                    echo("<p>");
                    echo ($row['deb']." / ".$row["fin"]);
                    echo("</p>");
                  }}
                  $CP=$paie->GetCNP($key['id'], $MoisChoisi);
                  if(sizeof($CP)!=0){
                  foreach($CP as $row){
                    echo("<p>");
                    echo ($row['deb']." / ".$row["fin"]);
                    echo("</p>");
                  }}
                  echo("</td>");
                }?>
        </tr>

        <tr><td>Nombre de jours de congés pris (payés + autres = total)</td>
              <?php foreach ($users as $key) {
              echo("<td class='inpaie'>");
                echo("<p>");
                $CP=$paie->GetCP($key['id'], $MoisChoisi);
                $nbCP=0;
                foreach($CP as $row){
                  $nbCP=$nbCP + $row['nb'];
                }
                  echo $nbCP;
                  echo " + ";
                  $CP=$paie->GetCNP($key['id'], $MoisChoisi);
                  $nbCNP=0;
                  foreach($CP as $row){
                    $nbCNP=$nbCNP + $row['nb'];
                  }
                  echo $nbCNP;
                  echo(" = ");
                  echo $nbCP+$nbCNP;
                  echo"</p>";
                  echo("</td>");
                }?>
        </tr>

        <tr><td>Nature des congés pris</td>
              <?php foreach ($users as $key) {
              echo("<td class='inpaie'>");
                $CP=$paie->GetCP($key['id'], $MoisChoisi);
                foreach($CP as $row){
                  echo("<p>");
                  echo ("Congés payés");
                  echo("</p>");
                }
                  $CP=$paie->GetCNP($key['id'], $MoisChoisi);
                  foreach($CP as $row){
                    echo("<p>");
                    echo ("Congés sans solde");
                    echo("</p>");
                  }
                echo"</td>";
                }?>
        </tr>

        <tr><td>Justification en cas de congé autre qu'un congé payé</td>
              <?php foreach ($users as $key) {
                $CP=$paie->GetCP($key['id'], $MoisChoisi);
              echo("<td class='inpaie'>");
                foreach($CP as $row){
                  echo("<p>");
                  echo ("/");
                  echo("</p>");
                }
                  $CP=$paie->GetCNP($key['id'], $MoisChoisi);
                  foreach($CP as $row){
                    echo("<p>");
                    echo ($row['justif']);
                    echo("</p>");
                  }
                  echo("</td>");
                }?>
        </tr>

        <tr><td>Jours de repos hebdomadaire sur la/les semaines incluant un jour de congé</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Date des jours de maladie</td>
              <?php
              foreach ($users as $key) {
                $maladie =$paie->GetMaladie($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($maladie)==0){echo "-";}
                  foreach($maladie as $row){
                    echo("<p>");
                    echo $row['deb']." / ".$row['fin'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Nombre de jours de maladie</td>
              <?php foreach ($users as $key) {
                $maladie =$paie->GetMaladie($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($maladie)==0){echo "<p>0</p>";}
                  foreach($maladie as $row){
                    echo("<p>");
                    echo $row['nb'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Date du dernier jour travaillé pour chaque arrêt maladie</td>
              <?php foreach ($users as $key) {
                $maladie =$paie->GetMaladie($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($maladie)==0){echo "-";}
                  foreach($maladie as $row){
                    echo("<p>");
                    echo $row['dernier_jour'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Jours de repos hebdomadaire sur la/les semaines incluant des jours de maladie</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Arrêt de travail fourni sous 48 heures ? Yes/No.</td>
              <?php foreach ($users as $key) {
                $maladie =$paie->GetMaladie($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($maladie)==0){echo "-";}
                  foreach($maladie as $row){
                    echo("<p>");
                    echo $row['48h'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Date des jours d'absence hors congé payé, congé sans solde et maladie</td>
              <?php foreach ($users as $key) {
                $absence =$paie->GetAbsence($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($absence)==0){echo "-";}
                  foreach($absence as $row){
                    echo("<p>");
                    echo $row['deb']." / ".$row['fin'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Nombre de jours d'absence hors congé payé, congé sans solde et maladie</td>
              <?php foreach ($users as $key) {
                $absence =$paie->GetAbsence($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($absence)==0){echo "0";}
                  foreach($absence as $row){
                    echo("<p>");
                    echo $row['nb'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Justification des jours d'absence hors congé payé, congé sans solde et maladie</td>
              <?php foreach ($users as $key) {
                $absence =$paie->GetAbsence($key['id'], $MoisChoisi);
                  echo("<td class='inpaie'>");
                  if(sizeof($absence)==0){echo "-";}
                  foreach($absence as $row){
                    echo("<p>");
                    echo $row['justif'];
                    echo("</p>");
                  }
                  echo ("</td>");
                }?>
        </tr>

        <tr><td>Jours de repos hebdomadaire sur la/les semaines incluant des jours d'absence</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Nombre d'heures de retard</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>

        <tr><td>Autre (rectification, cas non prévu, etc.) Merci de fournir le plus de détails possible.</td>
              <?php foreach ($users as $key) {
                  echo("<td class='inpaie'>X</td>");
                }?>
        </tr>
  </table>
</div id=Paies>





<br><br>
        </div>

        <a href="pointeuse.php" class="lienPages">Retour aux données</a>
        <br><br>
        </body>

        <script>
          function CheckClick(id){ //i de 0 à 13 suivi d'un espace
            if(document.getElementById(id).checked){
              len = id.length;
              lettre = id[(len-1)];
              deb = "";
              deb = id[0]+id[1];
              base="";

              if(deb%2==1 && lettre=="a"){
                deb=deb-1;
              }

              for (var i = 0; i < len-1; i++) {
                base=base+id[i];
              }
              if(lettre=="b"){// toggle le deuxieme
                base2=
              }

              if(lettre=="a"){
                paire=base+"b";
                document.getElementById(paire).checked=false;
              }
              else{
                paire=base+"a";
                document.getElementById(paire).checked=false;
              }

              if(lettre=="b"){

              }
            }
          }
        </script>

  <?php }
  else {
    header('Location: index.php');
    exit();
  }

}
      else {
        header('Location: index.php');
        exit();
      } ?>
</html>
