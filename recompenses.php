<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{
$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
$tables = $sqlite->getTableList();
$shifts = $sqlite->getShifts($_SESSION['id']);
$employees = $sqlite->getEmployees();
$shiftsOfEmployee = $sqlite->getShiftsForId($_SESSION["id"]);
$estChef = $sqlite->EstChef($_SESSION['id']);
$recompense = $sqlite->getRecompenses();
$mesPoints=$sqlite->getPoints($_SESSION['id']);
$recompenseAcquise = $sqlite->getRecompensesAcquises($_SESSION['id']);

?>



<script type="text/javascript">

/** Fonction basculant la visibilité d'un élément dom
* @parameter anId string l'identificateur de la cible à montrer, cacher
*/
function toggle(anId)
{
node = document.getElementById(anId);
if (node.style.visibility=="hidden")
{
// Contenu caché, le montrer
node.style.visibility = "visible";
node.style.height = "auto";			// Optionnel rétablir la hauteur
}
else
{
// Contenu visible, le cacher
node.style.visibility = "hidden";
node.style.height = "0";			// Optionnel libérer l'espace
}
}

</script>

<style> .monBody{
background-image: url("img6b.jpg"), linear-gradient(#858686, #090909);
background-repeat: no-repeat;
}
</style>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Récompenses</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body class="monBody">
      <div class="container">
          <div class="page-header"></br>
              <h1>Récompenses pour la ponctualité !</h1><br><br>
      <table class="table table-bordered">
          <thead>
              <tr>
                  <th class="colonne" style="width: 30%;">Seuil de points</th>
                  <th class="colonne">Récompense</th>
              </tr>
          </thead>
          <tbody>
                  <tr>
                    <?php foreach ($recompense as $shift) : ?>
                    <td class="case"><?php echo $shift['points'] ?></td>
                      <td class="case"><?php echo $shift['recompense'] ?></td>
                  </tr>
              <?php endforeach; ?>
          </tbody>
      </table>
      <br><br>
      <div style="margin-left: 2em; font-size: 25px;">
        <strong><underline style="text-decoration:underline">Mes points</underline> : <?php echo $mesPoints; ?></strong>
      </div><br>
      <h1>Récompenses acquises :</h1><br>
      <div>
        <table class="table table-bordered">
            <thead>
                <tr>
                  <th class="colonne" style="width: 30%;">Points</th>
                  <th class="colonne">Récompense</th>
              </tr>
          </thead>
          <tbody>
                  <tr>
                    <?php foreach ($recompenseAcquise as $shift) : ?>
                    <td class="case"><?php echo $shift['points'] ?></td>
                      <td class="case"><?php echo $shift['recompense'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
      </div>
      <a href="pointeuse.php" class='lienPages'>Retourner aux données</a>

  </div></div>
<?php }
else {
  header('Location: index.php');
  exit();
} ?>
    </body>
</html>
