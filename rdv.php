<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{
$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
$rdv = $sqlite->getRdv($_SESSION['id']);
$demandes = $sqlite->getRdvDemandes($_SESSION['id']);
$employees = $sqlite->getEmployees();
$nbOfEmployees=$sqlite->NbOfEmployees();
$passes=$sqlite->getRdvPasses($_SESSION['id']);

if(isset($_POST['NewRdv']) && $_POST['NewRdv']!=null){

  $date = $_POST['date'];
  $invites= $_POST['invites'];
  $type= $_POST['type'];
  $motif= $_POST['motif'];
  $deb= $_POST['deb'];
  $fin= $_POST['fin'];

if($date != null && $invites != null && $deb != null){
  $id_rdv = $sqlite->NewRdv($date, $type, $motif, $deb, $fin, $_SESSION['id']);

  if($id_rdv ==-1){
    echo('<script>window.alert("Demande déjà enregistrée");</script>');
        $repinsert = $sqlite->AjouterInvites($id_rdv, $invites, $_SESSION['id']);
        echo $repinsert;
  }
  else{
      $repinsert = $sqlite->AjouterInvites($id_rdv, $invites, $_SESSION['id']);
      echo $repinsert;
      $Lesinvites="";
      foreach ($invites as $inv) {
        $Lesinvites=$Lesinvites.$inv.", ";
      }

      $message = "La date : ".$date."\\nLes invités : ".$Lesinvites
                ."\\nType : ".$type." \\nMotif : ".$motif
                ."\\nL'heure de début : ".$deb."\\nL'heure de fin : ".$fin
                ."\\n\\nDemande enregistrée !\\n".$id_rdv;

                  ?>

                  <script>/*affiche le contenu de $message en php*/
                    var afficher = "<?php echo $message ?>";
                    window.alert(afficher);
                  </script>

                <?php

      header('Location: rdv.php');
      exit();
  }
}
else {echo"<script>window.alert(\"Merci de renseigner la date, les invités, et l\'heure de début au minimum\");</script>";}

}

if(isset($_POST['Supprimer']) && $_POST['Supprimer']!=null){
  $rep = $sqlite->DeleteRdv($_POST['id'], $_SESSION['id']);
  header('Location: rdv.php');
  exit();
}
if(isset($_POST['Repondre']) && $_POST['Repondre']!=null){
  $rep = $sqlite->RepRdv($_POST['id'], $_POST['rep'], $_SESSION['id']);
  header('Location: rdv.php');
  exit();
}

?>

<style> .monBody{
background-image: url("img6b.jpg"), linear-gradient(#858686, #090909);
background-repeat: no-repeat;
background-size: cover;
}
input{ width:80%;}
</style>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Rendez-vous</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>



    <body class="monBody">
      <div class="container">
          <div class="page-header"></br>
              <h1>Vos rendez-vous</h1><br><br>
              <h3>Demandés :</h3>
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonneId">id</th>
                          <th class="colonne">Demandeur</th>
                          <th class="colonne">Type</th>
                          <th class="colonne">Status</th>
                          <th class="colonne">Motif</th>
                          <th class="colonne">Date</th>
                          <th class="colonne">Début</th>
                          <th class="colonne">Fin</th>
                          <th class="colonne">Invités</th>
                          <th class="colonne">Décline</th>
                          <th class="colonne">Accepte</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($rdv as $shift) : ?>
                              <td class="caseid"><?php echo $shift['id'] ?></td>
                              <td class="case"><?php echo $shift['demandeur'] ?></td>
                              <td class="case"><?php echo $shift['type'] ?></td>
                              <td class="case"><?php echo $shift['status'] ?></td>
                              <td class="case"><?php echo $shift['motif'] ?></td>
                              <td class="case"><?php echo $shift['date'] ?></td>
                              <td class="case"><?php echo $shift['debut'] ?></td>
                              <td class="case"><?php echo $shift['fin'] ?></td>
                              <td class="case"><?php echo $shift['invites'] ?></td>
                              <td class="case"><?php echo $shift['refus'] ?></td>
                              <td class="case"><?php echo $shift['accepte'] ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              <br><br>
              <h3>Invité à :</h3>
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th class="colonneId">id</th>
                          <th class="colonne">Demandeur</th>
                          <th class="colonne">Type</th>
                          <th class="colonne">Status</th>
                          <th class="colonne">Motif</th>
                          <th class="colonne">Date</th>
                          <th class="colonne">Début</th>
                          <th class="colonne">Fin</th>
                          <th class="colonne">Invités</th>
                          <th class="colonne">Décline</th>
                          <th class="colonne">Accepte</th>
                      </tr>
                  </thead>
                  <tbody>
                          <tr>
                            <?php foreach ($demandes as $shift) : ?>
                              <td class="caseid"><?php echo $shift['id'] ?></td>
                              <td class="case"><?php echo $shift['demandeur'] ?></td>
                              <td class="case"><?php echo $shift['type'] ?></td>
                              <td class="case"><?php echo $shift['status'] ?></td>
                              <td class="case"><?php echo $shift['motif'] ?></td>
                              <td class="case"><?php echo $shift['date'] ?></td>
                              <td class="case"><?php echo $shift['debut'] ?></td>
                              <td class="case"><?php echo $shift['fin'] ?></td>
                              <td class="case"><?php echo $shift['invites'] ?></td>
                              <td class="case"><?php echo $shift['refus'] ?></td>
                              <td class="case"><?php echo $shift['accepte'] ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
<br>
<h3>Passés :</h3>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="colonneId">id</th>
            <th class="colonne">Demandeur</th>
            <th class="colonne">Type</th>
            <th class="colonne">Status</th>
            <th class="colonne">Motif</th>
            <th class="colonne">Date</th>
            <th class="colonne">Début</th>
            <th class="colonne">Fin</th>
            <th class="colonne">Invités</th>
            <th class="colonne">Décline</th>
            <th class="colonne">Accepte</th>
        </tr>
    </thead>
    <tbody>
            <tr>
              <?php foreach ($passes as $shift) : ?>
                <td class="caseid"><?php echo $shift['id'] ?></td>
                <td class="case"><?php echo $shift['demandeur'] ?></td>
                <td class="case"><?php echo $shift['type'] ?></td>
                <td class="case"><?php echo $shift['status'] ?></td>
                <td class="case"><?php echo $shift['motif'] ?></td>
                <td class="case"><?php echo $shift['date'] ?></td>
                <td class="case"><?php echo $shift['debut'] ?></td>
                <td class="case"><?php echo $shift['fin'] ?></td>
                <td class="case"><?php echo $shift['invites'] ?></td>
                <td class="case"><?php echo $shift['refus'] ?></td>
                <td class="case"><?php echo $shift['accepte'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<br>

  <div style="width: 40em; border: 1px solid #000000; background: radial-gradient(#666666,#232223); border-radius: 30px; color: #232223;">
    <fieldset style="margin-left:1em; width: 20em;">
      <legend style="color: white; text-shadow: grey;">Demander un RDV</legend>
        <form method="POST" action="rdv.php">
          <table cellspacing="5" style="width:35em; color: #B3B3B3;">
        <tr><td>

            <tr><td style="width:15em;">Type de rdv :</td><td style="width:20em;"> <input type="text" name="type" autocomplete="off"></td></tr>
            <tr><td>Motif :</td><td> <input type="text" name="motif" autocomplete="off"></td></tr>
            <tr><td>Date :</td><td> <input type="date" name="date" autocomplete="off"></td>
              <td rowspan="3"><input type="submit" name="NewRdv" class="rond" autocomplete="off"></td></tr>
            <tr><td>Début :</td><td> <input type="time" name="deb" autocomplete="off"></td></tr>
            <tr><td>Fin :</td><td> <input type="time" name="fin" autocomplete="off"></td></tr>
            <tr><td>Invités :</td><td style="padding-top: 5px;">
              <select type="checkbox" multiple="multiple" id="users" name="invites[]" size="<?php echo $nbOfEmployees; ?>">
                <?php foreach ($employees as $shift) : ?>
                  <option><?php echo $shift['username'] ?></option>
                <?php endforeach; ?>
              </select>
            </td></tr>
          </table></br>
        </form>
    </fieldset>
  </div>


<br><br>
  <div style="width: 40em; border: 1px solid #000000; background: radial-gradient(#666666,#232223); border-radius: 30px; color: #232223;">
            <fieldset style="margin-left:1em;">
              <legend style="color: white; text-shadow: grey;">Supprimer un RDV</legend>
              <form method="POST" action="rdv.php">
                <table cellspacing="5" style="width:35em; color: #B3B3B3;">
                  <tr><th style="width: 10em;"></th><th></th><th></th></tr>
                  <tr><td style="width:15em;">id du rdv :</td><td style="width:20em;"> <input type="text" name="id" autocomplete="off"></td>
                    <td rowspan="3"><input type="submit" name="Supprimer" class="rond" autocomplete="off"></td></tr>


                </table></br>
              </form>
            </fieldset>
  </div>
  <br><br>
    <div style="width: 40em; border: 1px solid #000000; background: radial-gradient(#666666,#232223); border-radius: 30px; color: #232223;">
              <fieldset style="margin-left:1em;">
                <legend style="color: white; text-shadow: grey;">Répondre à une demande</legend>
                <form method="POST" action="rdv.php">
                  <table cellspacing="5" style="width:35em; color: #B3B3B3;">
                    <tr><th style="width: 10em;"></th><th></th><th></th></tr>
                    <tr><td style="width:15em;">id du rdv :</td><td style="width:20em;"> <input type="text" name="id" autocomplete="off"></td>
                      <td rowspan="3"><input type="submit" name="Repondre" class="rond" autocomplete="off"></td></tr>
                      <tr><td>Réponse : </td><td>
                                  <input type=text list=champ name="rep">
                                  <datalist id=champ>
                                  <option>OK</option>
                                  <option>KO</option>
                                  </datalist></td><td colspan="3"></td></tr>


                  </table></br>
                </form>
              </fieldset>
    </div>

<br><br>
<a href="pointeuse.php" class='lienPages'>Retourner aux données</a>
<br><br>

</div></div>
<?php }
else {
  header('Location: index.php');
  exit();
} ?>
    </body>
</html>
