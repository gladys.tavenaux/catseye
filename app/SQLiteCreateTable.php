<?php

namespace App;

  include 'app/crypt.php';
/**
 * SQLite Create Table Demo
 */
class SQLiteCreateTable {

    /**
     * PDO object
     * @var \PDO
     */
    private $pdo;

    /**
     * connect to the SQLite database
     */
    public function __construct($pdo) {
        $this->pdo = $pdo;
    }







//   GENERAL   FUNCTIONS
    //   CONVERT    A   TIME    INTEGER   INTO   READABLE   TIME
    public function getReadableTimeWithSeconds($seconds) {
      $t = round($seconds);
      return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
    }

    //    USED   TO  GET  CURRENT   DATE   AND   TIME   AT   THE  DB   FORMAT
      function getDateTimeNow() {
          $tz_object = new \DateTimeZone("Europe/Amsterdam");
          //date_default_timezone_set('Brazil/East');

          $datetime = new \DateTime();
          $datetime->setTimezone($tz_object);
          return $datetime->format('Y\-m\-d\ h:i:s');
      }
    //    TO   USE   AT   EACH   MODIFICATION   OF   DB
    function ModifBDD($modif,$old,$new,$id, $table=null, $commentaire=null){
      $dated=$this->getDateTimeNow();
      //var_dump("modif ".$modif." old ".$old." new ".$new." id ".$id." table ".$table." commentaire ".$commentaire." date ".$dated);
      $stmt = $this->pdo->prepare('INSERT INTO Modification (Modif, Date, Old_Value, New_Value, Id_User, tableConcernee, Commentaire)
      VALUES (:modif, :dated, :old, :new, :id, :table, :commentaire);');
      $stmt->bindValue(":modif", $modif);
      $stmt->bindValue(":dated", $dated);
      $stmt->bindValue(":old", $old);
      $stmt->bindValue(":new", $new);
      $stmt->bindValue(":id", $id);
      $stmt->bindValue(":table", $table);
      $stmt->bindValue(":commentaire", $commentaire);

      $rep = $stmt->execute();
      if($rep==true || $rep==""){
      $stmt = $this->pdo->query("Select id from Modification where Date like '".$dated."' and Id_User=".$id.";");
      $eqp = [];
      while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
          $eqp[] = [
              'id' => $row['id']
          ];
      }
      return $eqp[0]['id'];
      }
      else{return -2;}
    }


    public function AnnuleModif($id){
      $stmt=$this->pdo->prepare('Delete from Modification where id='.$id);
      return $stmt->execute();
    }

    //      JUST    RETURNS   NUMBER   OF   EMPLOYEES
      public function NbOfEmployees(){
          $sql = ('Select Count(*) as nb from employee');
        $shifts = [];

          foreach ($this->pdo->query($sql) as $row) {
                 $shifts[] = [
                      'nb' => $row['nb']
                 ];
             }
             return $shifts[0]['nb'];
        }
        //            FORMAT   DATE ?
          function fputcsv2($fh, array $fields, $delimiter = ',', $enclosure = '"', $mysql_null = false) {
              $delimiter_esc = preg_quote($delimiter, '/');
              $enclosure_esc = preg_quote($enclosure, '/');

              $output = array();
              foreach ($fields as $field) {
                  if ($field === null && $mysql_null) {
                      $output[] = 'NULL';
                      continue;
                  }

                  $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
                      $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
                  ) : $field;
              }

              fwrite($fh, join($delimiter, $output) . "\n");
          }





    /**
     * create tables
     */
    public function createTables() {
        $commands = ['CREATE TABLE IF NOT EXISTS projects (
                        project_id   INTEGER PRIMARY KEY,
                        project_name TEXT NOT NULL
                      )',
            'CREATE TABLE IF NOT EXISTS tasks (
                    task_id INTEGER PRIMARY KEY,
                    task_name  VARCHAR (255) NOT NULL,
                    completed  INTEGER NOT NULL,
                    start_date TEXT,
                    completed_date TEXT,
                    project_id VARCHAR (255),
                    FOREIGN KEY (project_id)
                    REFERENCES projects(project_id) ON UPDATE CASCADE
                                                    ON DELETE CASCADE)'];
        // execute the sql commands to create new tables
        foreach ($commands as $command) {
            $this->pdo->exec($command);
        }
    }

    /**
     * get the table list in the database
     */
    public function getTableList() {

        $stmt = $this->pdo->query("SELECT name
                                   FROM sqlite_master
                                   WHERE type = 'table'
                                   ORDER BY name");
        $tables = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $tables[] = $row['name'];
        }

        return $tables;
    }

    /**
 * Get all shifts
 * @return type
 */






//         POINTEUSE.PHP          POINTEUSE.PHP          POINTEUSE.PHP          POINTEUSE.PHP          POINTEUSE.PHP          POINTEUSE.PHP          POINTEUSE.PHP
 //                       GET     SHIFTS
 public function getShifts($id) {
     $sql = ('SELECT s.id as \'shiftid\', e.username as \'Pseudo\', s.employee_id as \'id\', s.date, s.debut, s.fin, s.shift_duration as \'durée\', '
       .'s.pause_duration as \'temps de pause\', s.qa_formation_duration as \'Formation QA\' '
       .'FROM shifts s, employee e '
       .'INNER JOIN equipe_agent ea ON s.employee_id = ea.id_agent '
       .'INNER JOIN equipe_chef ec ON ea.id_equipe = ec.id_equipe '
       .'WHERE e.id=s.employee_id '
       .'AND ec.id_chef= '.$id.' ORDER BY date(date), debut');
     $shifts = [];

  foreach ($this->pdo->query($sql) as $row) {
         $shifts[] = [
              'shiftid' => $row['shiftid'],
             'Pseudo' => $row['Pseudo'],
             'id' => $row['id'],
             'date' => $row['date'],
             'debut' => $row['debut'],
             'fin' => $row['fin'],
             'durée' => $row['durée'],
             'temps de pause' => $row['temps de pause'],
             'Formation QA' => $row['Formation QA']
         ];
     }
     return $shifts;
 }
//      RETURNS   ALL   ROWS   IN   SHIFTS
 public function getAllShifts() {
     $sql = ('SELECT s.id as \'shiftid\', e.username as \'Pseudo\', s.employee_id as \'id\', s.date, s.debut, s.fin, s.shift_duration as \'durée\', '
       .'s.pause_duration as \'temps de pause\', s.qa_formation_duration as \'Formation QA\' '
       .'FROM shifts s, employee e WHERE s.employee_id = e.id ORDER BY date(date), debut');
     $shifts = [];

  foreach ($this->pdo->query($sql) as $row) {
         $shifts[] = [
              'shiftid' => $row['shiftid'],
             'Pseudo' => $row['Pseudo'],
             'id' => $row['id'],
             'date' => $row['date'],
             'debut' => $row['debut'],
             'fin' => $row['fin'],
             'durée' => $row['durée'],
             'temps de pause' => $row['temps de pause'],
             'Formation QA' => $row['Formation QA']
         ];
     }
     return $shifts;
 }
//         RETURN    ALL   SHIFTS   OF   THE   USER
public function getShiftsForId($id) {
  $sql=('SELECT debut, fin, shift_duration, pause_duration, date, qa_formation_duration '
          . 'FROM shifts where employee_id =' . $id.' ORDER BY date(date), debut');
      $shifts = [];
foreach ($this->pdo->query($sql) as $row) {
          $shifts[] = [
              'debut' => $row['debut'],
              'fin' => $row['fin'],
              'shift_duration' => $row['shift_duration'],
              'pause_duration' => $row['pause_duration'],
              'qa_formation_duration' => $row['qa_formation_duration'],
              'date' => $row['date']
          ];
      }
      return $shifts;
  }


/**
* Get array of employees infos
* @return type
*/

//                   GET    EMPLOYEES   AND    TEAM    STUFF
public function getEmployees() {
$stmt = $this->pdo->query('SELECT id, username, status '
        . 'FROM employee');
$employees = [];
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $employees[] = [
        'id' => $row['id'],
        'username' => $row['username'],
        'status' => $row['status']
    ];
}
return $employees;
}

public function getEquipe() {
$stmt = $this->pdo->query('SELECT id, Nom_Equipe '
        . 'FROM Equipe');
$eqp = [];
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $eqp[] = [
        'id' => $row['id'],
        'Nom_Equipe' => $row['Nom_Equipe']
    ];
}
return $eqp;
}
//         GET   NAME   OF    USER  FROM   HIS   ID
public function getUsernameById($employee_id, $tab_employees) {
  foreach ($tab_employees as $key => $employee) {
    if ($employee['id'] === $employee_id){
    return $employee['username'];}
  }
  return null;
}
//      GET   ID   OF   AN   EMPLOYEE  FROM  HIS   USERNAME
public function getIdFromUsername($name){
  $stmt = $this->pdo->query('SELECT id FROM employee where username =\''.$name.'\';');
  $count=0;
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'id' => $row['id']
      ];
      $count = $count+1;
}
if($count==0)
{
  return -1;
}
elseif ($count>1) {
  return -2;
}
elseif($count==1) {

  foreach ($employees as $employee) :
      return $employee['id'];
  endforeach;
}
else {
  return -2;
}
}
//        GET   THE   ID   OF   THE   TEAM
public function getIdFromEquipe($name){
  $stmt = $this->pdo->query('SELECT id FROM equipe where Nom_Equipe LIKE \''.$name.'\';');
  $count=0;
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'id' => $row['id']
      ];
      $count = $count+1;
}
if($count==0)
{
  return -1;
}
elseif ($count>1) {
  return -2;
}
elseif($count==1) {

  foreach ($employees as $employee) :
      return $employee['id'];
  endforeach;
}
else {
  return -2;
}
}

//       EASY    WAY   TO   KNOW   IF   USER   IS  TEAM   LEADER
public function EstChef($id) {

    $stmt = $this->pdo->query("SELECT id_equipe
                               FROM equipe_chef
                               WHERE id_chef = $id");
    $count = 0;
    $tables = [];
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $tables[] = $row['id_equipe'];
        $count=$count+1;
    }

    if ($count==0)
    {return false;}
    else {
      return true;
    }
}
//        EASY   WAY  TO  KNOW   IF  USER   IS   ADMINISTRATOR
public function EstAdmin($id) {

    $stmt = $this->pdo->query("SELECT id_employee
                               FROM admin
                               WHERE id_employee = $id");
    $count = 0;
    $tables = [];
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $tables[] = $row['id_employee'];
        $count=$count+1;
    }

    if ($count==0)
    {return false;}
    else {
      return true;
    }
}


//       INDEX.PHP           INDEX.PHP           INDEX.PHP           INDEX.PHP           INDEX.PHP           INDEX.PHP           INDEX.PHP           INDEX.PHP
//    USED   AT   index.php   TO   IDENTIFY   TO   THE   SITE
public function ConnectToSite($name, $pass) {

    $stmt = $this->pdo->query("SELECT id
                               FROM employee
                               WHERE password = $pass
                               AND username = $name");
    $count = 0;
    $tables = [];
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $tables[] = $row['id'];
        $count=$count+1;
    }

    if ($count==0)
    {return false;}
    else {
      return true;
    }
}

//     USED   TO   KNOW   IF   THE  PASSWORD   IS   THE   ONE   RECORDED   IN   DB
public function CompareMDP($mdpSaisi, $idUser)
{
    $stmt = $this->pdo->query("SELECT password
                               FROM employee
                               WHERE id = $idUser");
    $count = 0;
    $tables = [];
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $tables[] = $row['password'];
        $count=$count+1;
    }

    if ($count==0 || $count>1 || $mdpSaisi==$tables[0])
    {return "Error";}
    else {
      if (password_verify($mdpSaisi, $tables[0])==true) {
        return true;
      }
      else {
        return false;
      }
    }
  }






//        MODIFIER.PHP            MODIFIER.PHP            MODIFIER.PHP            MODIFIER.PHP            MODIFIER.PHP            MODIFIER.PHP            MODIFIER.PHP


//     USED   TO   SET   PASSWORD
public function InsertMDP($id, $clearPass) {
  $pass= Chiffrement::encrypt($clearPass);
sleep(0.5);
  $sql = 'UPDATE employee SET password = :pass WHERE id = :id;';

  $stmt = $this->pdo->prepare($sql);

          // passing values to the parameters
          $stmt->bindValue(':pass', $pass);
          $stmt->bindValue(':id', $id);

          // execute the update statement
          return $stmt->execute();
}
//       USED   TO   REGISTER   A   USER
  public function CreateUser($name, $clearPass) {
    if (isset($name) && $name != null && isset($clearPass) && $clearPass!=null){
      $pass = Chiffrement::encrypt($clearPass);
        $stmt = $this->pdo->prepare('INSERT INTO employee (username, status, password) VALUES (:name, \'déconnecté\', :pass)');
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":pass", $pass);
          $pass = Chiffrement::encrypt($clearPass);
                // execute the update statement
                return $stmt->execute();
    }
  }
//     USED   TO   MODIFICATE    AN   USER
public function ModifPseudo($id, $newpseudo){
  $sql=$this->pdo->prepare("update employee set username = \"$newpseudo\" where id=$id;");
  /*var_dump($sql);*/
  return $sql->execute();
}
public function ModifFullName($id, $fullname){
  $sql=$this->pdo->prepare("update employee set fullname = \"$fullname\" where id=$id;");
  /*var_dump($sql);*/
  return $sql->execute();
}
public function getFullName($id){
    $stmt = $this->pdo->query('SELECT fullname FROM employee where id = '.$id.';');
    $count=0;
    $employees = [];
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        $employees[] = [
            'id' => $row['fullname']
        ];
        $count = $count+1;
  }
  if($count==0)
  {
    return -1;
  }
  elseif ($count>1) {
    return -2;
  }
  elseif($count==1) {

    foreach ($employees as $employee) :
        return $employee['id'];
    endforeach;
  }
  else {
    return -2;
  }

}
//     USED   TO   CHANGE   SOMEONE'S   TEAM
public function ChangeEquipe($user, $action, $Equipe){
  if(isset($user) && isset($action) && isset($Equipe)
&& $user != null && $action != null && $Equipe != null){
  $id=$this->getIdFromUsername($user);
  $idEqui =$this->getIdFromEquipe($Equipe);
  if($id>0 && $idEqui>0){
    $stmt = $this->pdo->prepare('SELECT id_agent, id_equipe From equipe_agent WHERE id_agent=:id AND id_equipe=:idEqui;');
    $stmt->bindValue(":id", $id);
    $stmt->bindValue(":idEqui", $idEqui);
            $count = 0;
            $tables = [];
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $tables[] = $row['password'];
                $count=$count+1;
            }


  if ($action == "Ajouter à" && $id>0 && $idEqui>0 && $count==0 )
  {
    $stmt = $this->pdo->prepare('INSERT INTO equipe_agent (id_agent, id_equipe) VALUES (:id, :idEqui);');
    $stmt->bindValue(":id", $id);
    $stmt->bindValue(":idEqui", $idEqui);
            // execute the update statement
            return $stmt->execute();
  }
  elseif ($action == "Supprimer de" && $id>0 && $idEqui>0 && $count==0) {

      $stmt = $this->pdo->prepare('DELETE FROM equipe_agent WHERE id_agent=:id AND id_equipe=:idEqui');
      $stmt->bindValue(":id", $id);
      $stmt->bindValue(":idEqui", $idEqui);
              // execute the update statement
              return $stmt->execute();
  }
  else {
    return -3;
  }
}
else {
  return -2;
}
}
else {
  return -1;
}
}

//   USED   TO  CHANGE   TEAM   LEADER
public function ChangeChef($user, $action, $Equipe){
  if(isset($user) && isset($action) && isset($Equipe)
&& $user != null && $action != null && $Equipe != null){
  $id=$this->getIdFromUsername($user);
  $idEqui =$this->getIdFromEquipe($Equipe);
  if($id>0 && $idEqui>0){
    $stmt = $this->pdo->prepare('SELECT id_chef, id_equipe From equipe_chef WHERE id_chef=:id AND id_equipe=:idEqui;');
    $stmt->bindValue(":id", $id);
    $stmt->bindValue(":idEqui", $idEqui);
            $count = 0;
            $tables = [];
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $tables[] = $row['password'];
                $count=$count+1;
            }


  if ($action == "Ajouter à" && $id>0 && $idEqui>0 && $count==0 )
  {
    $stmt = $this->pdo->prepare('INSERT INTO equipe_chef (id_chef, id_equipe) VALUES (:id, :idEqui);');
    $stmt->bindValue(":id", $id);
    $stmt->bindValue(":idEqui", $idEqui);
            // execute the update statement
            return $stmt->execute();
  }
  elseif ($action == "Supprimer de" && $id>0 && $idEqui>0 && $count==0) {

      $stmt = $this->pdo->prepare('DELETE FROM equipe_chef WHERE id_chef=:id AND id_equipe=:idEqui');
      $stmt->bindValue(":id", $id);
      $stmt->bindValue(":idEqui", $idEqui);
              // execute the update statement
              return $stmt->execute();
  }
  else {
    return -3;
  }
}
else {
  return -2;
}
}
else {
  return -1;
}
}

//     USED   TO   GET   EMPLOYEES   OF   A   TEAM
function getAgentByTeam(){
  $stmt = $this->pdo->query('SELECT e.id, e.username as \'Pseudo\', eq.Nom_Equipe as \'Equipe\' '
  .'FROM employee e, equipe_agent ea, equipe eq '
.'WHERE eq.id=ea.id_equipe and e.id=ea.id_agent '
.'AND exists (select id_agent from equipe_agent where id_agent=e.id)'
.'GROUP BY e.id ORDER BY eq.Nom_Equipe, e.username');
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'id' => $row['id'],
          'Pseudo' => $row['Pseudo'],
          'Equipe' => $row['Equipe']
      ];
  }
  return $employees;
  }


//      USED   TO   GET  ALL  TEAM  LEADERS   AND   THEIR   TEAM
function getChefByTeam(){
  $stmt = $this->pdo->query('SELECT e.id, e.username as \'Chef\', eq.Nom_Equipe as \'Equipe\' '
  .'FROM employee e, equipe_chef ec, equipe eq '
.'WHERE eq.id=ec.id_equipe and e.id=ec.id_chef '
.'AND exists (select id_chef from equipe_chef where id_chef=e.id)'
.'GROUP BY e.id ORDER BY eq.Nom_Equipe, e.username');
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'id' => $row['id'],
          'Chef' => $row['Chef'],
          'Equipe' => $row['Equipe']
      ];
  }
  return $employees;
  }








//      RECOMPENSES.PHP                      RECOMPENSES.PHP                      RECOMPENSES.PHP                      RECOMPENSES.PHP                      RECOMPENSES.PHP





//      RETURNS   TABLE   OF   REWARDS
function getRecompenses(){
  $stmt = $this->pdo->query('SELECT * FROM recompenses order by points');
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'points' => $row['points'],
          'recompense' => $row['recompense']
      ];
  }
  return $employees;
}
//    GET    POINTS   OF   AN   USER
public function getPoints($id) {
    $sql = ('SELECT points from employee where id ='.$id);
    $shifts = [];
    $nb=0;


       foreach ($this->pdo->query($sql) as $row) {
              $shifts[] = [
                'points'=>$row['points']
        ];
        $nb=$nb+1;
    }

    if($nb==1)
    {
      if($shifts[0]['points']==null){return 0;}
      return $shifts[0]['points'];}
    else {
      return "Erreur";
    }
}
//       GET   REWARDS   EARNED    BY   USER
function getRecompensesAcquises($id){
  $pointsAcquis = $this->getPoints($id);

  $stmt = $this->pdo->query('SELECT * FROM recompenses where points <= '.$pointsAcquis);
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
      $employees[] = [
          'points' => $row['points'],
          'recompense' => $row['recompense']
      ];
  }
  return $employees;
  }










//                  RDV.PHP                                    RDV.PHP                                    RDV.PHP                                    RDV.PHP

//   GET   GUESTS   OF   A   RDV
public function getInvites($rdvId){
  $sql = ('SELECT id_employee from presence where id_rdv = '.$rdvId);
  $shifts = [];

  foreach ($this->pdo->query($sql) as $row) {
      $shifts[] = [
           'id_employee' => $row['id_employee'],
      ];
  }
  $first=true;
  $noms="";
foreach($shifts as $shift => $id){
  if($first){$noms=$this->getUsernameById($id['id_employee'], $this->getEmployees()); $first=false;}
  else{
    $noms = $noms.", ".$this->getUsernameById($id['id_employee'], $this->getEmployees());}
  }
  return $noms;
}

//    GET   GUESTS   WHO   RESPONDED   'OK'
public function getInvitesPresents($rdvId){
    $sql = ('SELECT id_employee from presence where (rep="OK" or rep="ok") and id_rdv = '.$rdvId);
    $shifts = [];

    foreach ($this->pdo->query($sql) as $row) {
        $shifts[] = [
             'id_employee' => $row['id_employee'],
        ];
    }
    $first=true;
    $noms="";
  foreach($shifts as $shift => $id){
    if($first){$noms=$this->getUsernameById($id['id_employee'], $this->getEmployees()); $first=false;}
    else{
      $noms = $noms.", ".$this->getUsernameById($id['id_employee'], $this->getEmployees());}
    }
    return $noms;
  }//            GET   GUESTS   WHO   RESPONDED   'KO'
  public function getInvitesAbsents($rdvId){
      $sql = ('SELECT id_employee from presence where (rep="ko" or rep="KO") and id_rdv = '.$rdvId);
      $shifts = [];

      foreach ($this->pdo->query($sql) as $row) {
          $shifts[] = [
               'id_employee' => $row['id_employee'],
          ];
      }
      $first=true;
      $noms="";
    foreach($shifts as $shift => $id){
      if($first){$noms=$this->getUsernameById($id['id_employee'], $this->getEmployees()); $first=false;}
      else{
        $noms = $noms.", ".$this->getUsernameById($id['id_employee'], $this->getEmployees());}
      }
      return $noms;
    }

//     GET   RDV   ASKED   BY   USER
public function getRdv($id) {
    $sql = ('SELECT distinct r.id, r.demandeur, r.type, r.status, r.motif, r.date, r.debut, r.fin '
      .'FROM rdv r, employee e '
      .'WHERE  r.id_demandeur = '.$id.' AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))<8 '
      .'ORDER BY date(r.date)');
    $shifts = [];


       foreach ($this->pdo->query($sql) as $row) {
              $invites = $this->getInvites($row['id']);
              $accepte = $this->getInvitesPresents($row['id']);
              $refus = $this->getInvitesAbsents($row['id']);
              $shifts[] = [
                  'id' =>$row['id'],
                   'demandeur' => $row['demandeur'],
                  'type' => $row['type'],
                  'status' => $row['status'],
                  'motif' => $row['motif'],
                  'date' => $row['date'],
                  'debut' => $row['debut'],
                  'fin' => $row['fin'],
                  'invites' => $invites,
                  'refus' => $refus,
                  'accepte' => $accepte,
        ];
    }
    return $shifts;
}
//      GET    RDV   WHERE    USER   IS   INVITED
public function getRdvDemandes($id) {
    $sql = ('SELECT distinct r.id, r.demandeur, r.type, r.status, r.motif, r.date, r.debut, r.fin '
      .'FROM rdv r, presence p '
      .'WHERE  p.id_employee = '.$id.' AND p.id_rdv = r.id AND (p.rep not like "ko" or p.rep not like "KO" or p.rep is null) AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))<8 '
      .'ORDER BY date(r.date)');
    $shifts = [];

    foreach ($this->pdo->query($sql) as $row) {
           $invites = $this->getInvites($row['id']);
           $accepte = $this->getInvitesPresents($row['id']);
           $refus = $this->getInvitesAbsents($row['id']);
           $shifts[] = [
                'id'=>$row['id'],
                'demandeur' => $row['demandeur'],
               'type' => $row['type'],
               'status' => $row['status'],
               'motif' => $row['motif'],
               'date' => $row['date'],
               'debut' => $row['debut'],
               'fin' => $row['fin'],
               'invites' => $invites,
               'refus' => $refus,
               'accepte' => $accepte,
     ];
    }

    return $shifts;
}

//     GET   PAST   RDV
public function getRdvPasses($id) {
    $sql = ('SELECT distinct r.id, r.demandeur, r.type, r.status, r.motif, r.date, r.debut, r.fin '
      .'FROM rdv r, presence p '
      .'WHERE  p.id_employee = '.$id.' AND p.id_rdv = r.id AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))>7 '
      .'AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))<20 ORDER BY date(r.date)');
    $shifts = [];

    foreach ($this->pdo->query($sql) as $row) {
           $invites = $this->getInvites($row['id']);
           $accepte = $this->getInvitesPresents($row['id']);
           $refus = $this->getInvitesAbsents($row['id']);
           $shifts[] = [
                'id'=>$row['id'],
                'demandeur' => $row['demandeur'],
               'type' => $row['type'],
               'status' => $row['status'],
               'motif' => $row['motif'],
               'date' => $row['date'],
               'debut' => $row['debut'],
               'fin' => $row['fin'],
               'invites' => $invites,
               'refus' => $refus,
               'accepte' => $accepte,
     ];
    }

    $sql = ('SELECT distinct r.id, r.demandeur, r.type, r.status, r.motif, r.date, r.debut, r.fin '
      .'FROM rdv r, employee e '
      .'WHERE  r.id_demandeur = '.$id.' AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))>7 '
      .' AND (Select Cast ((JulianDay(date(\'now\')) - JulianDay(r.date)) As Integer))<20 ORDER BY date(r.date)');


       foreach ($this->pdo->query($sql) as $row) {
              $invites = $this->getInvites($row['id']);
              $accepte = $this->getInvitesPresents($row['id']);
              $refus = $this->getInvitesAbsents($row['id']);
              $shifts[] = [
                  'id' =>$row['id'],
                   'demandeur' => $row['demandeur'],
                  'type' => $row['type'],
                  'status' => $row['status'],
                  'motif' => $row['motif'],
                  'date' => $row['date'],
                  'debut' => $row['debut'],
                  'fin' => $row['fin'],
                  'invites' => $invites,
                  'refus' => $refus,
                  'accepte' => $accepte,
        ];
    }

    return $shifts;
}
//     CREATE   A   RDV
    public function NewRdv($date, $type, $motif, $deb, $fin, $id) {
          $dateNow = $this->getDateTimeNow();
          $nom=$this->getUsernameById($id, $this->getEmployees());
          $nb=0;

          $sql = ('SELECT id from rdv where id_demandeur = '.$id.' and date like \''.$date.'\' and type like \''.$type
                 .'\' and motif like \''.$motif.'\' and debut like \''.$deb.'\' and fin like \''.$fin.'\';');

          foreach ($this->pdo->query($sql) as $row) {
                 $shifts[] = [
                     'id' =>$row['id']
           ];
           $nb=$nb+1;
          }


          if($nb==0)
          {
              $stmt = $this->pdo->prepare('INSERT INTO rdv (status, demandeur, id_demandeur, date, type, motif, debut, fin, date_demande) '
                                .'VALUES (\'nouveau\', \''.$nom.'\', '.$id.', \''.$date.'\', \''.$type.'\', \''.$motif.'\', \''.$deb.'\', \''.$fin.'\', \''.$dateNow.'\')');
              $stmt->execute();

              $sql = ('SELECT id from rdv where id_demandeur = '.$id.' and date_demande like \''.$dateNow.'\';');
              foreach ($this->pdo->query($sql) as $row) {
                ModifBDD("New RDV", "", $row['id'],$id);
                return $row['id'];
                 }
          }
          else {
            $sql = ('SELECT id from rdv where id_demandeur = '.$id.' and date like \''.$date.'\' and type like \''.$type
                   .'\' and motif like \''.$motif.'\' and debut like \''.$deb.'\' and fin like \''.$fin.'\';');
            foreach ($this->pdo->query($sql) as $row) {
              return $row['id'];
               }
          }
    }
//     ADD   GUESTS   TO   RDV
  public function AjouterInvites($idRdv, $idsInvites, $idOfUser){
    for ($i=0; $i < sizeof($idsInvites); $i++) {
      $idEmployee = $this->getIdFromUsername($idsInvites[$i]);
      /*var_dump("idEmployee: ".$idEmployee);*/
      $stmt=$this->pdo->prepare('Insert into presence (id_employee, id_rdv) VALUES '
                                 .'('.$idEmployee.', '.$idRdv.');');
      $stmt->execute();
      ModifBDD("Ajouter un invité","",$idEmployee, $idOfUser);
    }
  }
//       DELETE   A   RDV   CREATED   BY   USER
function DeleteRdv($id, $employee_id){
  $sql=$this->pdo->query("Select id from rdv where id=$id and id_demandeur=$employee_id");
  $rdvid=0;
  foreach($sql as $row){
    $rdvid=$row['id'];
  }

  if($rdvid>0){
    $sql=$this->pdo->prepare("Delete from presence where id_rdv = $rdvid");
    $sql->execute();
    $sql=$this->pdo->prepare("Delete from rdv where id=$rdvid");
    $sql->execute();

    ModifBDD("Delete RDV",$rdvid,"",$employee_id);
  }
}
//       RESPOND    TO   A   RDV   WHERE   USER    IS   INVITED
function RepRdv($id, $rep, $employee_id){

    $sql=$this->pdo->query("Select rep from presence where id_rdv=$id and id_employee =$employee_id;");
    $rdvid=0;
    foreach($sql as $row){
      $rdvid=$row['id'];
    }

    $this->ModifBDD("Rep rdv", $rdvid, $rep." à ".$id, $employee_id);

  $sql=$this->pdo->prepare("update presence set rep = \"$rep\" where id_rdv=$id and id_employee =$employee_id;");
  /*var_dump($sql);*/
  return $sql->execute();
}



//   ADMIN.PHP            ADMIN.PHP             ADMIN.PHP
function getModifications(){
  $stmt = $this->pdo->query(" select modification.Date, modification.modif as \"Modification\", modification.Old_Value as \"Ancienne valeur\",
  Modification.New_Value as \"Nouvelle valeur\", employee.username as \"Emetteur de la modification\" from
  modification inner join employee on modification.Id_User=employee.id
");
  $tables = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $tables[] = [
        'Date' => $row['Date'],
        'Modification' => $row['Modification'],
        'Ancienne valeur' => $row['Ancienne valeur'],
        'Nouvelle valeur' => $row['Nouvelle valeur'],
        'Emetteur de la modification' => $row['Emetteur de la modification']
    ];
  }

  return $tables;


}

function getAllAboutUsers(){
  $stmt = $this->pdo->query(' SELECT employee.id, employee.fullname, employee.username, employee.status, employee.points
                              from employee
                              GROUP BY employee.id
                              ORDER BY employee.username
                              ');
  $employees = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $currentid=$row['id'];

    $sqlequipe=$this->pdo->query("Select equipe.Nom_Equipe from equipe, equipe_agent where equipe.id=equipe_agent.id_equipe and equipe_agent.id_agent=".$row['id'].";");
    $Equipe="";
    foreach($sqlequipe as $rowequipe){
      $Equipe=$Equipe.$rowequipe['Nom_Equipe']." ";
    }

    if($this->EstChef($currentid)){
      $sqlchef=$this->pdo->query("Select equipe.Nom_Equipe from equipe, equipe_chef where equipe.id=equipe_chef.id_equipe and equipe_chef.id_chef=".$row['id'].";");
      $equipes="";
      foreach($sqlchef as $rowchef){
        $equipes=$equipes.$rowchef['Nom_Equipe']." + ";
      }
      $Chef=$equipes;
    }
    else{$Chef="-";}

    if( $this->EstAdmin($currentid)){
    $Admin = "Oui";}
    else{$Admin="-";}

      $employees[] = [
          'id' => $currentid,
          'fullname' => $row['fullname'],
          'username' => $row['username'],
          'status' => $row['status'],
          'points' => $row['points'],
          'Nom_Equipe' => $Equipe,
          'Chef' => $Chef,
          'Admin' => $Admin
      ];
  }
  return $employees;
}


//            EMPLOIDUTEMPS.PHP               EMPLOIDUTEMPS.PHP               EMPLOIDUTEMPS.PHP               EMPLOIDUTEMPS.PHP               EMPLOIDUTEMPS.PHP               EMPLOIDUTEMPS.PHP
function RepToInshifts($DateLundi, int $i, $id){
  if($i%2 == 0){
    $type="matin";
  }
  else {
    $type="aprem";
  }

  $unJour = 3600 * 24; // nombre de secondes dans une journée


$DateLundiConvert = str_replace('/', '-', $DateLundi);
$DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+(($i/2)*$unJour);

  $sql=$this->pdo->query("select exists(
                            select couleur
                            from preference
                            where date=\"".date("Y-m-d", $date)."\" and type like \"$type\" and id_employee=$id
                          ) as \"reponse\";");

  foreach($sql as $row){
    $rep=$row['reponse'];
  }

  if($rep==1){
    return"X";
  }
  elseif ($rep==0) {
    return"-";
  }
  else{return "error";}


}
function ColorInshiftsOf($DateLundi, $i, $id){
  if($i%2 == 0){
    $type="matin";
  }
  else {
    $type="aprem";
  }

  $unJour = 3600 * 24; // nombre de secondes dans une journée


$DateLundiConvert = str_replace('/', '-', $DateLundi);
$DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+(($i/2)*$unJour);

  $sql=$this->pdo->query("select exists(
                            select couleur
                            from preference
                            where date=\"".date("Y-m-d", $date)."\" and type like \"$type\" and id_employee=$id
                          ) as \"reponse\";");

  foreach($sql as $row){
    $rep=$row['reponse'];
  }

  if($rep==1){
    $sql2=$this->pdo->query("select couleur as rep
                              from preference
                              where date=\"".date("Y-m-d", $date)."\" and type like \"$type\" and id_employee=$id
                              ;");
                              $rep2="";
  foreach($sql2 as $row2){
    $rep2=$row2['rep'];
  }
    return $rep2;
  }
  elseif ($rep==0) {
    return"pink";
  }
  else{return "black";}

}


function InsertPreference($DateLundi, $i, $value, $id){
  if($i%2 == 0){
    $type="matin";
  }
  else {
    $type="aprem";
  }

  $unJour = 3600 * 24; // nombre de secondes dans une journée


  $DateLundiConvert = str_replace('/', '-', $DateLundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+(($i/2)*$unJour);
  $date=date("Y-m-d", $date);

  if($value=="Oui"){$couleur="green";}
  elseif($value=="Non"){$couleur="red";}
  else{$couleur=$value;}

    $sql2=$this->pdo->query("select couleur as rep
                              from preference
                              where date=\"".$date."\" and type like \"$type\" and id_employee=$id
                              ;");
    $rep2="";
    foreach($sql2 as $row2){
      $rep2=$row2['rep'];
    }
      if($rep=="blue"){
        return -2;
      }
  else{
       //return($date." ".$type." " .$couleur." ".$id);
      $sql=$this->pdo->prepare("insert into preference ('date', 'type', 'couleur', 'id_employee')
    select '$date', '$type', '$couleur', $id
    where not exists (select * from preference where date like '$date' and type like '$type'
                                and couleur like '$couleur' and id_employee = $id)");
      return $sql->execute();
    }
}

//    MATHILDE.PHP
/*$old=$sqlite->getThisPreference($day, $shift);
$sqlite->ModifBDD("Suppression de préférence ".$day." ".$shift, $old,"",$_SESSION['id']);
$sqlite->DeletePreference($day, $shift);*/
public function getThisPreference($day, $shift, $id){
  $sql = ('Select * from preference where date like \''.$day.'\' and type like \''.$shift.'\' and id_employee='.$id.';');
  $shifts = [];
  $string="";
  foreach ($this->pdo->query($sql) as $row) {
         $shifts[] = [
           'id' =>$row['id'],
           'id_employee'=>$row['id_employee'],
           'date'=>$row['date'],
           'type'=>$row['type'],
           'couleur'=>$row['couleur']
         ];
     }
     $string="id :".$shifts[0]['id']." id_employee :".$shifts[0]['id_employee']." date :"
            .$shifts[0]['date']." type :".$shifts[0]['type']." couleur :".$shifts[0]['couleur'];
     return $string;
}

public function DeletePreference($day, $shift, $id){
  $stmt = $this->pdo->prepare('DELETE FROM preference WHERE id_employee='.$id.' AND date like "'.$day.'" and type like "'.$shift.'"');
          return $stmt->execute();
}
public function InsertRecompense($points, $recompense){
  $sql=$this->pdo->prepare("INSERT INTO recompenses VALUES($points, '$recompense')");
  return $sql->execute();
}

}
?>
