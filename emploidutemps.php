<?php
session_start();
require 'vendor/autoload.php';

use App\SQLiteConnection as SQLiteConnection;
use App\SQLiteCreateTable as SQLiteCreateTable;

if(isset($_SESSION['id']) && $_SESSION['id'] != null)
{


$sqlite = new SQLiteCreateTable((new SQLiteConnection())->connect());
// create new tables
//$sqlite->createTables(); Crée une vieille table de tutoriel (SQLiteCreateTable.php)
// get the table list
$tables = $sqlite->getTableList();
$shifts = $sqlite->getShifts($_SESSION['id']);
$employees = $sqlite->getEmployees();
$shiftsOfEmployee = $sqlite->getShiftsForId($_SESSION["id"]);
$allShifts = $sqlite->getAllShifts();
$estChef = $sqlite->EstChef($_SESSION['id']);
$estAdmin = $sqlite->EstAdmin($_SESSION['id']);
$users=$sqlite->getAllAboutUsers();


$CeLundi = date('d/m/Y', strtotime('last monday'));
$DateLundiProchain =date('d/m/Y',strtotime('next monday'));
$DateDebutShifts = date('d/m/Y',strtotime('14 january 2019'));

function SemaineSuivante($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)+($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

function SemainePrecedente($Lundi){
  $unJour = 3600 * 24; // nombre de secondes dans une journée
  $uneSemaine = $unJour * 7;

  $DateLundiConvert = str_replace('/', '-', $Lundi);
  $DateLundiConvert = date("Y-m-d", strtotime($DateLundiConvert));

  $date=strtotime($DateLundiConvert)-($uneSemaine);
  $date=date("d/m/Y", $date);
  return $date;
}

if(!isset($DateLundi) || $DateLundi == null)
{$DateLundi = $CeLundi;}

if (isset($_POST['ChoixSemaine']) && $_POST['ChoixSemaine'] !=null){
  $DateLundiProchain = $_POST['semaine'];
  $DateLundi = SemainePrecedente($DateLundiProchain);
}
else{$DateLundiProchain=SemaineSuivante($DateLundi);}

if(isset($_POST['preference']) && $_POST['preference'] != null){
  $tableau=[];
  for ($i=0; $i < 14 ; $i++) {
    if(isset($_POST[$i])){
      $tableau[]=[$i, $_POST[$i]];
      /*echo '<script>window.alert("Réponse : '.implode(", ",$tableau[$i]).'")</script>';*/
    }
  }

  foreach ($tableau as $row => $value) {
    $rep=$sqlite->InsertPreference($_POST['lundi'], $tableau[$row][0], $tableau[$row][1], $_SESSION['id']);
    if($rep==-2){echo '<script>window.alert("Valeur non modifiable: shift définitif établi")</script>';}
  }
  $DateLundi=$_POST['lundi'];
}
?>
<style>
  body{
    background-image: url("ressources/img6b.jpg"), linear-gradient(#858686, #090909);
    background-repeat: no-repeat;
    background-size: cover;
  }

</style>



<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="TAVENAUX Gladys">
        <title>Emploi du temps</title>
        <link href="monCSS.css" rel="stylesheet">
        <link href="http://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    </head>




    <body>
    <div id="debug"></div>
        <div class="container">
            <div class="page-header"></br>
              <?php
              $BoucleLundi=$DateDebutShifts;
              $DateLundiProchain=SemaineSuivante($DateLundiProchain);
              echo $DateDebutShifts." - ".$DateLundiProchain;
              ?>
              <form method="post" style="width: 15em; margin-left: 2em;">
                <h5>Choix de la semaine</h5>
                <input type=text list=lundis name="semaine" autocomplete="off">
                <datalist id=lundis >
                  <?php
                    while (strtotime($BoucleLundi) != strtotime(SemaineSuivante($DateLundiProchain))) {
                      echo "<option>".$BoucleLundi."</option>";
                      $BoucleLundi=SemaineSuivante($BoucleLundi);
                    }
                  ?>
                </datalist>
                <input type="submit" name="ChoixSemaine">
                </form>
              <h2>Gestion des shifts</h2>
              <h5>Choix de la semaine : <?php echo $DateLundi;?></h5>



              <table class="shifts">
                <tr>
                  <td colspan="2">Utilisateur :</td>
                  <?php foreach ($users as $shift) : ?>
                  <td style="padding: 0 0.7em 0 0.7em;"><?php echo $shift['username'] ?></td>
                  <?php endforeach; ?>
                </tr>
                <tr>
                  <td rowspan="2">Lundi</td>
                  <td>matin</td>

                  <?php foreach ($users as $shift) : ?>
                  <!-- <td><?php //echo $shift['id'] ?></td>-->
                    <td rowspan="14">
                      <table class="inshifts">
                        <?php for ($i=0; $i < 14; $i++) {?>
                          <tr><td style="color: <?php echo $sqlite->ColorInshiftsOf($DateLundi, $i, $shift['id']);?>; font-weight: bold; font-size: 27px;"><?php echo $sqlite->RepToInshifts($DateLundi, $i, $shift['id']);?></td></tr><!--il faut 14 lignes-->
                        <?php } ?>
                      </table>
                    </td>
                  <?php endforeach; ?>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Mardi</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Mercredi</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Jeudi</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Vendredi</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Samedi</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
                <tr>
                  <td rowspan="2">Dimanche</td>
                  <td>matin</td>
                </tr>
                <tr>
                  <td>aprem</td>
                </tr>
              </table>
<span>
<fieldset>
  <legend>
      <p><u>Légende :</u></p>
      <p><span style="color: blue">X</span> : Shift définitif</p>
      <p><span style="color: yellow">X</span> : Repos</p>
      <p><span style="color: green">X</span> : Shift souhaité</p>
      <p><span style="color: red">X</span> : Shift non souhaité</p>
      <p><span style="color: black">X</span> : Congés</p>
    </legend>
</fieldset>
</span>

<br><br><br>
  <h2>Mes préférences</h2>
  <h5>Choix de la semaine : <i style="color: #452057"><?php echo $DateLundi;?></i></h5>
  <i style="color:white; opacity: 0.7;">Ne rien cocher si le shift concerné ne vous dérange pas</i>
  <form method="POST">
    <input style="visibility: hidden;" name="lundi" value='<?php echo $DateLundi; ?>'>
    <table class="no-border"><tr><td>
    <table cellspacing="5" class="shifts">
      <tr><td>Jour</td><td>Shift</td><td style="width: 10em;">Choix</td></tr>
      <td rowspan="2">Lundi</td>
      <td>matin</td>
      <td rowspan="14">
        <table class="inshifts" id="monTableau" style="text-align: left;">
          <?php for ($i=0; $i < 14; $i++) {?>
            <tr><td>
              <input type="checkbox" name="<?php echo $i; ?>" id="<?php echo $i; ?>a"
              value="Oui" onclick="CheckClick(this.id)" style="padding: 10px; margin-left: 25px;">
              <span>😻</span></input>
            </td>
            <td>
              <input type="checkbox" name="<?php echo $i; ?>" id="<?php echo $i; ?>b"
              value="Non" onclick="CheckClick(this.id)" style="margin-left: 1em; padding: 10px;">
              <span>😰</span></input>
            </td></tr>
          <?php } ?>
        </table>
      </td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Mardi</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Mercredi</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Jeudi</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Vendredi</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Samedi</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
    <tr>
      <td rowspan="2">Dimanche</td>
      <td>matin</td>
    </tr>
    <tr>
      <td>aprem</td>
    </tr>
  </table></td><td><input type="submit" name="preference" class="rond" autocomplete="off"></td></tr></table>

  </br>
  </form>
<a href="pointeuse.php" class="lienPages">Retour aux données</a>
            </div>
        </div>
      <?php }
      else {
        header('Location: index.php');
        exit();
      } ?>

      <script>
        function CheckClick(id){
          if(document.getElementById(id).checked){
            len = id.length;
            lettre = id[(len-1)];
            base="";
            for (var i = 0; i < len-1; i++) {
              base=base+id[i];
            }

            if(lettre=="a"){paire=base+"b";}
            else{paire=base+"a";}
            document.getElementById(paire).checked=false;
          }
        }
      </script>
      <br><br>
    </body>


</html>
